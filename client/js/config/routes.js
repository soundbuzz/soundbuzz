import Home from '../components/pages/Home';
import NotFound from '../components/pages/NotFound';
import MainLayout from '../components/layouts/MainLayout';
import Login from '~/components/pages/Login';
import Signup from "~/components/pages/Signup";
import Search from "~/components/pages/Search";
import Upload from "~/components/pages/Upload";
import Radios from "~/components/pages/Radios";
import Moderation from "~/components/pages/Moderation";
import Genre from "../components/pages/Genre";
import Onegenre from "../components/pages/Onegenre";
import Favorites from "../components/pages/Favorites";
import Recovery from "../components/pages/Recovery";
import Trends from "../components/pages/Trends";
import Recent from "../components/pages/Recent";
import Manage from "../components/pages/Manage";
import Settings from "../components/pages/Settings";

export default {
  root: {
    path: '/',
    page: Home,
    //layout: MainLayout,
    isPrivate: false
  },
  login: {
    path: '/login',
    page: Login,
    isPrivate: false
  },
  signup: {
    path: '/signup',
    page: Signup,
    isPrivate: false
  },
  search: {
    path: '/search',
    page: Search,
    isPrivate: false
  },
  upload: {
    path: '/upload',
    page: Upload,
    isPrivate: true
  },
  radios: {
    path: '/radios',
    page: Radios,
    isPrivate: false
  },
  moderation: {
    path: '/mod',
    page: Moderation,
    isPrivate: true
  },
  recovery: {
    path: '/recovery',
    page: Recovery,
    isPrivate: false
  },
  genre: {
    path: '/genre',
    page: Genre,
    isPrivate: false
  },
  onegenre: {
    path: '/genre/:genre_id',
    page: Onegenre,
    isPrivate: false
  },
  favorites: {
    path: '/favorites',
    page: Favorites,
    isPrivate: true
  },
  trends: {
    path: '/trends',
    page: Trends,
    isPrivate: false
  },
  recent: {
    path: '/recent',
    page: Recent,
    isPrivate: false
  },
  manage: {
    path: '/manage',
    page: Manage,
    isPrivate: true
  },
  settings: {
    path: '/settings',
    page: Settings,
    isPrivate: true
  },
  notFound: {
    path: '*',
    page: NotFound,
    isPrivate: false
  }
};
