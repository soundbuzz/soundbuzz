import i18next from 'i18next';
import languageDetector from 'i18next-browser-languagedetector';
import en from '~/i18n/en';
import fr from '~/i18n/fr';

const i18nextInstance = i18next.use(languageDetector).init({
  // lng: 'en', // for debug purpose only
  debug: true,
  fallbackLng: 'en',
  resources: {
    en: {
      translation: en
    },
    fr: {
      translation: fr
    }
  }
});

export default i18nextInstance;
