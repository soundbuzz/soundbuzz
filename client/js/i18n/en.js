import * as c from '~/i18n/constants';

export default {
  [c.HELLO]: 'hello world',
  [c.HOME]: 'Home',
  [c.TRENDING]: 'Trending',
  [c.RECENTLY_PLAYED]: 'History',
  [c.GENRE]: 'Genre',
  [c.RADIOS]: 'Radios',
  [c.PLAYLISTS]: 'Playlists',
  [c.LOG_IN]: 'Log In',
  [c.USERNAME]: 'Username',
  [c.PASSWORD]: 'Password',
  [c.REMEMBER]: 'Remember',
  [c.SIGN_UP]: 'Sign UP',
  [c.LOG_OUT]: 'Log Out',
  [c.FAVORITES]: 'Favorites',
  [c.ADD_PLAYLIST]: 'Add Playlist',
  [c.PAGE_NOT_FOUND]: 'Page not found',
  [c.SEARCH]: 'Search...',
  [c.SEARCH_ON_SOUNDBUZZ]: 'Search on Soundbuzz',
  [c.PAGE_NOT_FOUND_DESCRIPTION]: 'Maybe the page you are looking for has been removed, or you typed in the wrong URL.',
  [c.SEARCH_ON_SOUNDBUZZ_DESCRIPTION]: 'Find your favorite songs, artists, albums, radios and playlists.',
  [c.READ]: 'Read',
  [c.EMAIL]: 'Email',
  [c.AGREE_TERMS]: 'I agree the terms and conditions.',
  [c.REPEAT_PASSWORD]: 'Repeat Password',
};
