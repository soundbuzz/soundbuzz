import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from "@material-ui/core/styles";
import {login} from '~/model/actions/auth';
import Typography from "@material-ui/core/Typography";
import SearchIcon from '@material-ui/icons/Search';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as tc from '~/i18n/constants';
import {translate} from "react-i18next";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import IconButton from '@material-ui/core/IconButton';
import SaveAltIcon from "@material-ui/core/SvgIcon/SvgIcon";
import {setCurrentMusic} from "~/model/actions/player";

const styles = theme => ({
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchIcon: {
    fontSize: 80,
    marginBottom: 10
  },
  searchTypo: {
    fontSize: 32,
    marginBottom: 16
  },
  secondSearchTypo: {
    fontSize: 16,
  },
  card: {
    flexGrow: 1,
    marginBottom: theme.spacing.unit * 2
  }
});

const mapStateToProps = state => ({
  search: state.search,
  auth: state.auth
});

class Search extends React.Component {
  constructor(props) {
    super(props);

    this.playMusic = this.playMusic.bind(this);
  }

  playMusic(musicId){
    this.props.dispatch(setCurrentMusic(musicId, this.props.auth.userId));
  }

  render() {
    return (
      <Choose>
        <When condition={!this.props.search.isSearching && this.props.search.results === null}>
          <div className={this.props.classes.root}>
            <div>
              <Typography align='center' variant="title" color="textSecondary" className={this.props.classes.searchTypo}>
                <SearchIcon className={this.props.classes.searchIcon}/>
                <br/>
                Search on Soundbuzz {this.props.search.value}
              </Typography>
              <Typography align='center' variant="body1" color="textSecondary" className={this.props.classes.secondSearchTypo}>
                Find your favorite songs, artists, albums, radios and playlists.
              </Typography>
            </div>
          </div>
        </When>
        <When condition={this.props.search.pending}>
          <div className={this.props.classes.root}>
            <div>
              <CircularProgress size={50} />
            </div>
          </div>
        </When>
        <When condition={this.props.search.error}>
          <div className={this.props.classes.root}>
            <div>
              <span>Ooops there is an error</span>
            </div>
          </div>
        </When>
        <When condition={this.props.search.results !== null}>
          <div style={{flexGrow: 1}}>
            <Card className={this.props.classes.card}>
              <CardContent style={{paddingBottom: 16}}>
                <Typography variant="title" color="textSecondary">
                  <SearchIcon style={{verticalAlign: 'text-top'}}/> Resultats
                </Typography>
              </CardContent>
            </Card>
            <For each="track" index="idx" of={this.props.search.results}>
              <Card key={idx} className={this.props.classes.card}>
                <CardContent style={{display: 'flex', paddingBottom: 16}}>
                  <div className="photo">
                    <img style={{height: 48}} src={track.photo}/>
                  </div>
                  <div className="title" style={{marginLeft: 24, width: 200}}>
                    <Typography variant="subheading" color="textPrimary" style={{lineHeight: '48px'}}>
                      {track.title}
                    </Typography>
                  </div>
                  <div className="artist" style={{marginLeft: 24, flexGrow: 1}}>
                    <Typography variant="subheading" color="textSecondary" style={{lineHeight: '48px'}}>
                      {track.artist}
                    </Typography>
                  </div>
                  <div className="play">
                    <IconButton color='primary' component="a" onClick={() => {this.playMusic(track.id)}}>
                      <PlayArrowIcon/>
                    </IconButton>
                  </div>
                </CardContent>
              </Card>
            </For>
          </div>
          {/*<pre>{JSON.stringify(this.props.search.results)}</pre>*/}
        </When>
      </Choose>
    );
  }
}

export default connect(mapStateToProps)(translate()(withStyles(styles)(Search)));
