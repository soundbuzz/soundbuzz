// react
import React from 'react';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from "@material-ui/core/Typography/Typography";
import Button from '@material-ui/core/Button';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {setCurrentMusic} from "~/model/actions/player";
import connect from "react-redux/es/connect/connect";
import {translate} from "react-i18next";
import {withStyles} from "@material-ui/core";

const mapStateToProps = state => ({
  auth: state.auth
});

/*
* SoundUnite
* 
* Ce composant affiche les détails d'une musique, lesquels lui seront passés lors de son appel.
*/
class SoundUnite extends React.Component {
  constructor(props){
    super(props);

    this.playMusic = this.playMusic.bind(this);
  }

  playMusic(musicId){
    this.props.dispatch(setCurrentMusic(musicId, this.props.auth.userId));
  }


// ** render **
	render() {

		return (
			<div className="Genre" id="Genre">
          <Card>
              <CardContent>
                  <Typography variant="headline">
                      {this.props.details.title}
                  </Typography>
                  <Typography variant="subheading" color="textSecondary">
                  {this.props.details.description}
                  </Typography>
                <Button onClick={() => {this.playMusic(this.props.details.id)}} variant="outlined" color="primary" style={{marginLeft: 'auto', marginTop: 16}}>
                  Lire
                </Button>
              </CardContent>
          </Card>
          <br/>
			</div>
		)
	}
}

export default connect(mapStateToProps)(SoundUnite);
