import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TitleIcon from '@material-ui/icons/Title';
import QueueMusicIcon from '@material-ui/icons/QueueMusic';
import FormatAlignLeftIcon from '@material-ui/icons/FormatAlignLeft';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import AudiotrackIcon from '@material-ui/icons/Audiotrack';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import PersonIcon from '@material-ui/icons/Person';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as tc from '~/i18n/constants';
import {translate} from "react-i18next";
import FormControl from "@material-ui/core/FormControl/FormControl";
import Grid from "@material-ui/core/Grid/Grid";
import EmailIcon from "@material-ui/core/SvgIcon/SvgIcon";
import TextField from "@material-ui/core/TextField/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import Button from "~/components/atoms/Button";
import Paper from "@material-ui/core/Paper/Paper";
import DateIcon from '@material-ui/icons/DateRange';
import FormHelperText from '@material-ui/core/FormHelperText';
import env from '~/config/env';
import {push} from 'connected-react-router'
import routes from '~/config/routes';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import {setOwner} from '~/model/actions/auth';

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

const styles = theme => ({
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  wrapper: {
    margin: 'auto',
    padding: 20,
    marginBottom: 20
  },
  fieldWrapper: {
    margin: theme.spacing.unit,
  },
  icon: {
    margin: theme.spacing.unit,

  },
  button: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit,
    width: '100%'
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  uploadInput: {
    display: 'none',
  },
  errorWrapper: {
    display: 'block',
    margin: theme.spacing.unit,
  }
});

const mapStateToProps = state => ({
  auth: state.auth
});

class Upload extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formErrors: [],
      pending: false,
      selectedGenre: 1,
      genres: []
    };

    this.audioUploadInput = React.createRef();
    this.pictureUploadInput = React.createRef();
    this.titleInput = React.createRef();
    this.genreInput = React.createRef();
    this.descInput = React.createRef();
    this.artisteInput = React.createRef();
    this.composerInput = React.createRef();
    this.authDownloadInput = React.createRef();
    this.explicitInput = React.createRef();
    this.dateInput = React.createRef();

    this.onSubmit = this.onSubmit.bind(this);
    this.handleGenreChange = this.handleGenreChange.bind(this);
  }

  componentWillMount(){
    fetch(`${env.API_BASE_URL}genre?limit=-1`, {
      method: "GET",
      headers: {
        'Accept': 'application/json'
      },
    }).then(response => {
      return response.json()
    }).then(genres => {
      console.log(genres);
      this.setState({
        genres
      })
    });
  }

  onSubmit(event) {
    event.preventDefault();

    let that = this;

    this.setState({
      formErrors: [],
      pending: true
    });

    let formErrors = [];

    if (this.audioUploadInput.current.files == null || this.audioUploadInput.current.files.length <= 0) {
      formErrors.push('You must select an audio file')
    }

    if (this.pictureUploadInput.current.files == null || this.pictureUploadInput.current.files.length <= 0) {
      formErrors.push('You must select a picture')
    }

    if (this.titleInput.current.value.length <= 0) {
      formErrors.push('You must set a title')
    }

    if (this.dateInput.current.value.length <= 0) {
      formErrors.push('You must set a creation date')
    }

    if (this.genreInput.current.value.length <= 0) {
      formErrors.push('You must set a genre')
    }

    if (this.descInput.current.value.length <= 0) {
      formErrors.push('You must set a description')
    }

    if (this.artisteInput.current.value.length <= 0) {
      formErrors.push('You must set an artiste')
    }


    if (this.composerInput.current.value.length <= 0) {
      formErrors.push('You must set a composer')
    }

    if (formErrors.length > 0) {
      this.setState({
        formErrors,
        pending: false
      });

      return;
    }

//    this.authDownloadInput = React.createRef();


    let audioFile = this.audioUploadInput.current.files[0];
    let formData = new FormData();

    formData.append('sound_file', audioFile, audioFile.name);

    getBase64(this.pictureUploadInput.current.files[0]).then((picture64) => {
      fetch(`${env.API_BASE_URL}api/v1/sound/sound-detail`, {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "photo": picture64,
          "title": this.titleInput.current.value,
          "description": this.descInput.current.value,
          "artist": this.artisteInput.current.value,
          "song_writer": this.composerInput.current.value,
          "explicit_content": this.explicitInput.current.checked === true,
          "authorization_download": this.authDownloadInput.current.checked === true,
          "user_id": this.props.auth.userId,
          "genre_id":  this.state.selectedGenre,
          "create_date": this.dateInput.current.value
        })
      }).then(function (response) {
        return response.json();
      })
      .then(function (blob) {
        console.log(blob);
        let songId = blob.id;

        fetch(`${env.API_BASE_URL}api/v1/sound/upload?id=${songId}`, {
          method: "POST",
          body: formData
        }).then(() => {
          that.props.dispatch(setOwner());
          that.props.dispatch(push(routes.root.path));
        });
      });
    });
  }

  handleGenreChange(event){
    this.setState({
      selectedGenre: event.target.value
    });
  }

  render() {
    return (
      <div className={this.props.classes.root}>
        <Paper className={this.props.classes.wrapper}>
          <Typography gutterBottom align='center' variant="title" color="textSecondary">
            Upload <CloudUploadIcon className={this.props.classes.rightIcon} style={{verticalAlign: 'bottom'}}/>
          </Typography>
          <br/>
          <form onSubmit={this.onSubmit}>
            <FormControl fullWidth>
              <input
                ref={this.audioUploadInput}
                accept="audio/*"
                className={this.props.classes.uploadInput}
                id="contained-button-audio-file"
                type="file"
              />
              <label htmlFor="contained-button-audio-file" style={{display: 'inline-flex', flexDirection: 'column'}}>
                <Button disabled={this.state.pending} variant="contained" component="span" color="primary" className={this.props.classes.button}>
                  Upload audio file
                  <AudiotrackIcon className={this.props.classes.rightIcon}/>
                </Button>
              </label>

              <input
                ref={this.pictureUploadInput}
                accept="image/*"
                className={this.props.classes.uploadInput}
                id="contained-button-image-file"
                type="file"
              />
              <label htmlFor="contained-button-image-file" style={{display: 'inline-flex', flexDirection: 'column'}}>
                <Button disabled={this.state.pending} variant="contained" component="span" color="primary" className={this.props.classes.button}>
                  Upload picture
                  <AddPhotoAlternateIcon className={this.props.classes.rightIcon}/>
                </Button>
              </label>

              <div className={this.props.classes.fieldWrapper}>
                <Grid container spacing={8} alignItems="flex-end">
                  <Grid item>
                    <TitleIcon color="primary"/>
                  </Grid>
                  <Grid item style={{flexGrow: 1}}>
                    <TextField
                      disabled={this.state.pending}
                      style={{width: '100%'}}
                      label="Title"
                      type="text"
                      autoFocus={true}
                      inputRef={this.titleInput}
                    />
                  </Grid>
                </Grid>
              </div>
              <div className={this.props.classes.fieldWrapper}>
                <Grid container spacing={8} alignItems="flex-end">
                  <Grid item>
                    <QueueMusicIcon color="primary"/>
                  </Grid>
                  <Grid item style={{flexGrow: 1}}>
                    <Select
                      disabled={this.state.pending}
                      value={this.state.selectedGenre}
                      onChange={this.handleGenreChange}
                      input={<Input style={{width: '100%', marginTop: 16}} label="Genre" inputRef={this.genreInput}/>}
                    >
                      <For each="genre" index="idx" of={this.state.genres}>
                        <MenuItem key={idx} value={genre.id}>{genre.name}</MenuItem>
                      </For>
                    </Select>
                  </Grid>
                </Grid>
              </div>
              <div className={this.props.classes.fieldWrapper}>
                <Grid container spacing={8} alignItems="flex-end">
                  <Grid item>
                    <FormatAlignLeftIcon color="primary"/>
                  </Grid>
                  <Grid item style={{flexGrow: 1}}>
                    <TextField
                      disabled={this.state.pending}
                      style={{width: '100%'}}
                      label="Description"
                      multiline
                      type="text"
                      inputRef={this.descInput}
                    />
                  </Grid>
                </Grid>
              </div>
              <div className={this.props.classes.fieldWrapper}>
                <Grid container spacing={8} alignItems="flex-end">
                  <Grid item>
                    <PersonIcon color="primary"/>
                  </Grid>
                  <Grid item style={{flexGrow: 1}}>
                    <TextField
                      disabled={this.state.pending}
                      style={{width: '100%'}}
                      label="Artiste"
                      type="text"
                      inputRef={this.artisteInput}
                    />
                  </Grid>
                </Grid>
              </div>
              <div className={this.props.classes.fieldWrapper}>
                <Grid container spacing={8} alignItems="flex-end">
                  <Grid item>
                    <PersonIcon color="primary"/>
                  </Grid>
                  <Grid item style={{flexGrow: 1}}>
                    <TextField
                      disabled={this.state.pending}
                      style={{width: '100%'}}
                      label="Compositeur"
                      type="text"
                      inputRef={this.composerInput}
                    />
                  </Grid>
                </Grid>
              </div>

              <div className={this.props.classes.fieldWrapper}>
                <Grid container spacing={8} alignItems="flex-end">
                  <Grid item>
                    <DateIcon color="primary"/>
                  </Grid>
                  <Grid item style={{flexGrow: 1}}>
                    <TextField
                      disabled={this.state.pending}
                      style={{width: '100%'}}
                      label="Date de création"
                      type="date"
                      inputRef={this.dateInput}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </Grid>
                </Grid>
              </div>

              <div className={this.props.classes.fieldWrapper}>
                <FormControlLabel
                  control={
                    <Checkbox
                      disabled={this.state.pending}
                      color="primary"
                      inputRef={this.authDownloadInput}
                    />
                  }
                  label="Autorisation de téléchargement"
                />
              </div>
              <div className={this.props.classes.fieldWrapper}>
                <FormControlLabel
                  control={
                    <Checkbox
                      disabled={this.state.pending}
                      color="primary"
                      inputRef={this.explicitInput}
                    />
                  }
                  label="Contient du contenue explicit"
                />
              </div>
              <FormHelperText error style={{marginTop: 0}}>
                <For each="error" index="idx" of={this.state.formErrors}>
                  <span key={idx} className={this.props.classes.errorWrapper}>{error}</span>
                </For>
              </FormHelperText>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                loading={this.state.pending}
                className={this.props.classes.button}
              >
                Upload
              </Button>
            </FormControl>
          </form>
        </Paper>
      </div>
    );
  }
}

export default connect(mapStateToProps)(translate()(withStyles(styles)(Upload)));
