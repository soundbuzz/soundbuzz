import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import {login as loginAction} from '~/model/actions/auth';
import Button from "~/components/atoms/Button";
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import LockIcon from '@material-ui/icons/Lock';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import env from '~/config/env';
import {push} from "connected-react-router";
import routes from "~/config/routes";
import FormHelperText from "@material-ui/core/FormHelperText/FormHelperText";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const mapStateToProps = state => ({
  auth: state.auth
});

const styles = (theme) => ({
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  wrapper: {
    margin: 'auto',
    padding: 20,
    marginBottom: 20
  },
  fieldWrapper: {
    margin: theme.spacing.unit,
  },
  icon: {
    margin: theme.spacing.unit,

  },
  button: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit,
    width: '100%'
  },
  errorWrapper: {
    display: 'block',
    margin: theme.spacing.unit,
  }
});

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formErrors: [],
      pending: true
    };

    this.usernameInput = React.createRef();
    this.passwordInput = React.createRef();
    this.rememberInput = React.createRef();

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event){
    event.preventDefault();

    let that = this;

    this.setState({
      formErrors: [],
      pending: true
    });

    let formErrors = [];

    if (this.usernameInput.current.value.length <= 0) {
      formErrors.push('You must put a username');
    }

    if (this.passwordInput.current.value.length <= 0) {
      formErrors.push('You must put a password');
    }

    if (formErrors.length > 0) {
      this.setState({
        formErrors,
        pending: false
      });

      return;
    }

    this.props.dispatch(loginAction(this.usernameInput.current.value, this.passwordInput.current.value, this.rememberInput.current.checked));
  }

  render() {
    return (
      <div className={this.props.classes.root}>
        <Paper className={this.props.classes.wrapper}>
          <Typography gutterBottom align='center' variant="title" color="textSecondary">
            Login
          </Typography>
          <form onSubmit={this.onSubmit}>
            <FormControl fullWidth>
              <div className={this.props.classes.fieldWrapper}>
                <Grid container spacing={8} alignItems="flex-end">
                  <Grid item>
                    <AccountCircleIcon color="primary"/>
                  </Grid>
                  <Grid item>
                    <TextField
                      label="Username"
                      type="text"
                      autoComplete="current-username"
                      autoFocus={true}
                      inputRef={this.usernameInput}
                      disabled={this.props.auth.pending}
                    />
                  </Grid>
                </Grid>
              </div>
              <div className={this.props.classes.fieldWrapper}>
                <Grid container spacing={8} alignItems="flex-end">
                  <Grid item>
                    <LockIcon color="primary"/>
                  </Grid>
                  <Grid item>
                    <TextField
                      label="Password"
                      type="password"
                      autoComplete="current-password"
                      inputRef={this.passwordInput}
                      disabled={this.props.auth.pending}
                    />
                  </Grid>
                </Grid>
              </div>
              <div className={this.props.classes.fieldWrapper}>
                <FormControlLabel
                  control={
                    <Checkbox
                      color="primary"
                      inputRef={this.rememberInput}
                    />
                  }
                  label="Remember"
                />
              </div>
              <FormHelperText error style={{marginTop: 0}}>
                <For each="error" index="idx" of={this.state.formErrors}>
                  <span key={idx} className={this.props.classes.errorWrapper}>{error}</span>
                </For>
                <If condition={this.props.auth.fail}>
                  <span className={this.props.classes.errorWrapper}>Wrong username or password</span>
                </If>
              </FormHelperText>

              <Button
                variant="contained"
                color="primary"
                type="submit"
                loading={this.props.auth.pending}
                className={this.props.classes.button}
              >
                Login
              </Button>
              <Button
                variant="outlined"
                color="primary"
                component={Link}
                to={routes.recovery.path}
                style={{margin: 8, width: '100%'}}
              >
                Mot de passe oublié ?
              </Button>
            </FormControl>
          </form>
        </Paper>
      </div>
    );
  }
}

export default connect(mapStateToProps)(withStyles(styles)(Login));
