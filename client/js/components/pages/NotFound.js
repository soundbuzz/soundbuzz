import React from 'react';
import Typography from '@material-ui/core/Typography';
import {withStyles} from "@material-ui/core/styles";

const styles = (theme) => ({
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

class NotFound extends React.Component {
  render() {
    return (
      <div className={this.props.classes.root}>
        <div>
          <Typography gutterBottom align='center' variant="title" color="textSecondary">
            Page not found :(
          </Typography>
          <Typography align='center' variant="body1" color="textSecondary">
            Maybe the page you are looking for has been removed, or you typed in the wrong URL
          </Typography>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(NotFound);
