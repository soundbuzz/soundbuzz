import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from "@material-ui/core/styles";
import {login} from '~/model/actions/auth';
import Typography from "@material-ui/core/Typography";
import SearchIcon from '@material-ui/icons/Search';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as tc from '~/i18n/constants';
import {translate} from "react-i18next";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import LockIcon from '@material-ui/icons/Lock';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import SaveAltIcon from "@material-ui/core/SvgIcon/SvgIcon";
import {setCurrentMusic} from "~/model/actions/player";
import env from "~/config/env";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';

const styles = theme => ({
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchIcon: {
    fontSize: 80,
    marginBottom: 10
  },
  searchTypo: {
    fontSize: 32,
    marginBottom: 16
  },
  secondSearchTypo: {
    fontSize: 16,
  },
  card: {
    flexGrow: 1,
    marginBottom: theme.spacing.unit * 2
  }
});

const mapStateToProps = state => ({
  search: state.search,
  auth: state.auth
});

class Manage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tracks: []
    };

    this.onDelete = this.onDelete.bind(this);
  }

  componentWillMount(){
    this.updateList();
  }

  updateList(){
    fetch(`${env.API_BASE_URL}sound`, {
      method: "GET",
      headers: {
        'Accept': 'application/json',
      }
    }).then(function (response) {
      return response.json();
    }).then(tracks => {
      console.log(tracks);
      this.setState({
        tracks
      });
    });
  }

  onDelete(musicId){
    fetch(`${env.API_BASE_URL}api/v1/sound/delete`, {
      method: "DELETE",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: musicId
      })
    }).then(response => {
      this.updateList();
    });
  }

  render() {
    return (
      <div style={{flexGrow: 1}}>
        <Card className={this.props.classes.card}>
          <CardContent style={{paddingBottom: 16}}>
            <Typography variant="title" color="textSecondary">
              <LibraryBooksIcon style={{verticalAlign: 'text-top'}}/> Gestion des musiques
            </Typography>
          </CardContent>
        </Card>
        <For each="track" index="idx" of={this.state.tracks.reverse()}>
          <Card key={idx} className={this.props.classes.card}>
            <CardContent style={{display: 'flex', paddingBottom: 16}}>
              <div className="photo">
                <img style={{height: 48}} src={track.photo}/>
              </div>
              <div className="title" style={{marginLeft: 24, width: 200}}>
                <Typography variant="subheading" color="textPrimary" style={{lineHeight: '48px'}}>
                  {track.title}
                </Typography>
              </div>
              <div className="artist" style={{marginLeft: 24, flexGrow: 1}}>
                <Typography variant="subheading" color="textSecondary" style={{lineHeight: '48px'}}>
                  {track.artist}
                </Typography>
              </div>
              <div className="play">
                <IconButton color='primary' component="a" onClick={() => {this.onDelete(track.id)}}>
                  <DeleteIcon/>
                </IconButton>
              </div>
            </CardContent>
          </Card>
        </For>
      </div>
    );
  }
}

export default connect(mapStateToProps)(translate()(withStyles(styles)(Manage)));
