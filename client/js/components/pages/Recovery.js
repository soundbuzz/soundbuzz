import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import {login as loginAction} from '~/model/actions/auth';
import Button from "~/components/atoms/Button";
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import EmailIcon from '@material-ui/icons/Email';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import LockIcon from '@material-ui/icons/Lock';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormHelperText from "@material-ui/core/FormHelperText/FormHelperText";
import env from '~/config/env';
import {push} from "connected-react-router";
import routes from "~/config/routes";

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const mapStateToProps = state => ({
  auth: state.auth
});

const styles = (theme) => ({
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  wrapper: {
    margin: 'auto',
    padding: 20,
  },
  fieldWrapper: {
    margin: theme.spacing.unit,
  },
  icon: {
    margin: theme.spacing.unit,

  },
  button: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit,
    width: '100%'
  },
  errorWrapper: {
    display: 'block',
    margin: theme.spacing.unit,
  }
});

class Recovery extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formErrors: [],
      pending: false
    };

    this.emailInput = React.createRef();
    this.usernameInput = React.createRef();
    this.passwordInput = React.createRef();
    this.passwordRepeatInput = React.createRef();
    this.acceptTermsInput = React.createRef();

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();

    let that = this;

    this.setState({
      formErrors: [],
      pending: true
    });

    let formErrors = [];

    if (this.emailInput.current.value.length <= 0) {
      formErrors.push('You must set an email')
    } else if (!validateEmail(this.emailInput.current.value)) {
      formErrors.push('Incorrect email')
    }

    if (formErrors.length > 0) {
      this.setState({
        formErrors,
        pending: false
      });

      return;
    }

    //fetch()
    fetch(`${env.API_BASE_URL}api/v1/rescu/password`, {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "email": this.emailInput.current.value,
      })
    }).then(function(response) {
      that.props.dispatch(push(routes.login.path));
    });
  }

  render(){
    return (
      <div className={this.props.classes.root}>
        <Paper className={this.props.classes.wrapper}>
          <Typography gutterBottom align='center' variant="title" color="textSecondary">
            Recovery password
          </Typography>
          <form onSubmit={this.onSubmit}>
            <FormControl fullWidth>
              <div className={this.props.classes.fieldWrapper}>
                <Grid container spacing={8} alignItems="flex-end">
                  <Grid item>
                    <EmailIcon color="primary"/>
                  </Grid>
                  <Grid item style={{flexGrow: 2}}>
                    <TextField
                      style={{width: '100%'}}
                      label="Email"
                      type="email"
                      autoComplete="current-email"
                      autoFocus={true}
                      inputRef={this.emailInput}
                      disabled={this.state.pending}
                    />
                  </Grid>
                </Grid>
              </div>
              <FormHelperText error style={{marginTop: 0}}>
                <For each="error" index="idx" of={this.state.formErrors}>
                  <span key={idx} className={this.props.classes.errorWrapper}>{error}</span>
                </For>
              </FormHelperText>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                loading={this.state.pending}
                className={this.props.classes.button}
                disabled={this.state.pending}
              >
                Send password
              </Button>
            </FormControl>
          </form>
        </Paper>
      </div>
    );
  }
}

export default connect(mapStateToProps)(withStyles(styles)(Recovery));
