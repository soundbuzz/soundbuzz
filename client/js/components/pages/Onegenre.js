import React from 'react';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import routes from '~/config/routes';
import Grid from "@material-ui/core/Grid";
import Paper from '@material-ui/core/Paper';
import {withStyles} from "@material-ui/core/styles";
import {login} from '~/model/actions/auth';
import {translate} from 'react-i18next';
import * as tc from '~/i18n/constants'; // tc = translation constants
import SoundUnite from './SoundUnite';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from "@material-ui/core/Typography/Typography";
import IconButton from "@material-ui/core/IconButton/IconButton";
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import env from '~/config/env';
import {setCurrentMusic} from '~/model/actions/player';

// Intégrer le style de cette page
const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  card: {
    display: 'flex',
    marginBottom: theme.spacing.unit * 2
  },
  details: {
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 300,
    height: 150,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing.unit * 3
  },
  playIcon: {
    height: 38,
    width: 38,
  },
});

const mapStateToProps = state => (state);

/*
* Onegenre
*
* Cette page affiche les chansons contenues dans un genre qui correspond à l'ID passé en paramètre.
*/
class Onegenre extends React.Component {
  constructor(props) {
    super(props);

    this.playTrack = this.playTrack.bind(this);
  }

  playTrack(event){
    this.props.dispatch(setCurrentMusic(1));
  }

  /*
  * getGenreId()
  *
  * Cette fonction récupère l'ID du genre à partir de l'URL.
  */
  getGenreId() {
    var idgenre = this.props.router.location.pathname.replace('/genre/', '');

    this.setState({
        isLoaded: false,
        idgenre,
        sounds: []
    });

  }

  /*
  * getSounds($id)
  * $id : ID du genre à partir duquel on souhaite récupérer les chansons
  *
  * Cette fonction récupère les chansons du genre correspondant à l'ID passé en paramètre.
  */
  getSounds($id) {

    // Utilisation de notre API pour récupérer les chansons
    fetch(env.API_BASE_URL+"sound?genre_id=" + $id + "&limit=-1")
      .then(res => res.json())
      .then(

        // S'il y a des résultats: mettre à jour le State
        (result) => {
          this.setState({
            isLoaded: true,
            sounds: result
          });

        },

        // En cas d'erreur...
        (error) => {
          this.setState({
              isLoaded: true,
              error
            });
        }
      );

  }

  /*
  * Traitement a effectuer avant que le composant (autrement dit, la page) ne soit monté
  */
  componentWillMount(){
    this.getGenreId(); // Récupérer l'ID du genre via la getGenreId()

  }

  /*
  * Traitement a effectuer après que le composant ait été monté
  */
  componentDidMount(){
        this.getSounds(this.state.idgenre); // Récupérer la liste des chansons appartenant au genre dont l'ID est passé en paramètre (en provenance du State)

  }

  render() {

    // Mainetnant stockons dans une constante ce qui contiendra la liste des musiques de ce genre

    const sounds = Object
      .keys(this.state.sounds)
      .map(key => <SoundUnite key={key} details={this.state.sounds[key]} />)
    ;
    // sounds va contenir une concaténation de rendus du composant "SoundUnite", auquel on aura passé les détails de chaque chanson.


    return (

      <div className={this.props.classes.root}>

        { sounds.length < 1 ?
          <center>
            <Typography variant="headline">
              Mince !
            </Typography>
            <Typography variant="subheading" color="textSecondary">
              Il n'y a pas de chansons disponibles pour le moment dans cette catégorie.
            </Typography>
          </center>
        :
          <div>
            {sounds}
           </div>
        }



      </div>
    );
  }
}

export default connect(mapStateToProps)(translate()(withStyles(styles, {withTheme: true})(Onegenre)));
