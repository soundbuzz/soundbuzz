import React from 'react';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import routes from '~/config/routes';
import Grid from "@material-ui/core/Grid";
import Paper from '@material-ui/core/Paper';
import {withStyles} from "@material-ui/core/styles";
import {login} from '~/model/actions/auth';
import {translate} from 'react-i18next';
import * as tc from '~/i18n/constants'; // tc = translation constants
import GenreUnite from './GenreUnite';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from "@material-ui/core/Typography/Typography";
import IconButton from "@material-ui/core/IconButton/IconButton";
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import env from '~/config/env';
import {setCurrentMusic} from '~/model/actions/player';

// Intégrer le style de cette page
const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  card: {
    display: 'flex',
    marginBottom: theme.spacing.unit * 2
  },
  details: {
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 300,
    height: 150,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing.unit * 3
  },
  playIcon: {
    height: 38,
    width: 38,
  },
});

const mapStateToProps = state => (state);

/*
* Genre
*
* Cette page affiche la liste des genres de musique.
*/
class Genre extends React.Component {
  constructor(props) {
    super(props);

    this.playTrack = this.playTrack.bind(this);
  }

  playTrack(event){
    this.props.dispatch(setCurrentMusic(1));
  }

  /*
  * Traitement a effectuer avant que le composant (autrement dit, la page) ne soit monté
  */
  componentWillMount(){

    // Initialiser les variables contenus dans le State
    this.setState({
        isLoaded: false,
        items: []
      });

    // Récupérer les genres à partir de notre API
    fetch(env.API_BASE_URL+"genre?limit=-1")
        .then(res => res.json())
        .then(
            (result) => {
                // Résultat : on met à jour le state
                this.setState({
                    isLoaded: true,
                    items: result
                  });

            },

            (error) => {
                // Sinon : on log l'erreur
                this.setState({
                    isLoaded: true,
                    error
                  });

            }
        );

  }

  render() {

		// On fait une constante qui va contenir la liste des genres
		const genres = Object
			.keys(this.state.items)
			.map(key => <GenreUnite key={key} details={this.state.items[key]} />)
		;
    // sounds va contenir une concaténation de rendus du composant "GenreUnite". Chacun d'eux reçoit les détails du genre courant durant le parsing.

    return (

      <div className={this.props.classes.root}>

        {genres}

      </div>
    );
  }
}

export default connect(mapStateToProps)(translate()(withStyles(styles, {withTheme: true})(Genre)));
