// react
import React from 'react';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from "@material-ui/core/Typography/Typography";
import Button from '@material-ui/core/Button';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

/*
* GenreUnite
* 
* Ce composant affiche les détails d'un genre, lesquels lui seront passés lors de son appel.
*/
class GenreUnite extends React.Component {

// ** render **
	render() {

		return (
			<div className="Genre" id="Genre">
                <Card>
                    <CardContent style={{paddingBottom: 16}}>
                      <div style={{display: 'flex', alignItems: 'center'}}>
                        <div style={{flexGrow: 1}}>
                          <Typography variant="headline">
                            {this.props.details.name}
                          </Typography>
                        </div>
                        <div>
                          <Button
                            variant="outlined"
                            color="primary"
                            component={Link}
                            to={`/genre/${this.props.details.id}`}
                          >
                            En savoir plus
                          </Button>
                        </div>
                      </div>
                    </CardContent>
                </Card>
                <br/>
			</div>
		)
	}
}

export default GenreUnite;
