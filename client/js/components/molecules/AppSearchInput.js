import React from 'react';
import PropTypes from 'prop-types';

import SearchIcon from '@material-ui/icons/Search';
import {fade} from '@material-ui/core/styles/colorManipulator';
import {withStyles} from '@material-ui/core/styles';
import connect from "react-redux/es/connect/connect";
import routes from "../../config/routes";
import {push} from 'connected-react-router'
import {search} from '~/model/actions/search'
import _ from 'lodash';
import {translate} from "react-i18next";
import * as tc from '~/i18n/constants';

const mapStateToProps = state => ({
  router: state.router
});

const styles = (theme) => {
  const light = theme.palette.type === 'light';
  const placeholder = {
    color: theme.palette.common.white,
    opacity: 0.4,
    transition: theme.transitions.create('opacity', {
      duration: theme.transitions.duration.shorter,
    }),
  };
  return {
    root: {
      fontFamily: theme.typography.fontFamily,
      position: 'relative',
      marginRight: theme.spacing.unit * 2,
      marginLeft: theme.spacing.unit * 4,
      borderRadius: 2,
      background: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        background: fade(theme.palette.common.white, 0.25),
      },
      '& $input': {
        //transition: theme.transitions.create('width'),
        // width: theme.mixins.searchInput.width,
        // '&:focus': {
        //   width: theme.mixins.searchInput.width + 50,
        // },
      },
    },
    search: {
      paddingTop: 2,
      width: theme.spacing.unit * 7,
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    input: {
      font: 'inherit',
      padding: `${theme.spacing.unit}px ${theme.spacing.unit}px ${theme.spacing.unit}px ${theme
        .spacing.unit * 7}px`,
      border: 0,
      display: 'block',
      verticalAlign: 'middle',
      whiteSpace: 'normal',
      background: 'none',
      margin: 0, // Reset for Safari
      color: 'inherit',
      width: '100%',
      '&:focus': {
        outline: 0,
      },
      // '&::placeholder': {
      //   textOverflow: 'ellipsis !important',
      //   color: 'blue'
      // }
      '&::-webkit-input-placeholder': placeholder,
      '&::-moz-placeholder': placeholder, // Firefox 19+
      '&:-ms-input-placeholder': placeholder, // IE 11
      '&::-ms-input-placeholder': placeholder, // Edge
    },
  };
};

class AppSearchInput extends React.Component {
  constructor(props) {
    super(props);

    this.onFocus = this.onFocus.bind(this);
    this.onInput = this.onInput.bind(this);

    this.inputDebounce = _.debounce((value)=>{
      this.onInput(value);
    }, 400);
  }

  onFocus(event){
    // if current route is not already search
    if(this.props.router.location.pathname !== routes.search.path){
      this.props.dispatch(push(routes.search.path));
    }
  }

  onInput(value){
    this.props.dispatch(search(value));
  }

  componentWillUnmount(){
    this.inputDebounce.cancel();
  }

  render() {
    const {classes} = this.props;

    return (
      <div className={classes.root + ' ' + this.props.className}>
        <div className={classes.search}>
          <SearchIcon/>
        </div>
        <input
          id="search-input"
          className={classes.input}
          placeholder={this.props.t(tc.SEARCH)}
          //type='search'
          onFocus={this.onFocus}
          onInput={event => {this.inputDebounce(event.target.value)}}
        />
      </div>
    );
  }
}

AppSearchInput.propTypes = {
  classes: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(translate()(withStyles(styles)(AppSearchInput)));
