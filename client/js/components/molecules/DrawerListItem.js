import React from "react";
import ListItem from '@material-ui/core/ListItem';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import {withStyles} from "@material-ui/core/styles";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import {Link} from "react-router-dom";
import {connect} from 'react-redux';

const mapStateToProps = state => ({
  router: state.router
});

const styles = (theme) => ({
  nested: {
    paddingLeft: theme.spacing.unit * 6
  },
  active: {
    backgroundColor: theme.palette.action.hover,
  }
});

const DrawerListItem = props => {
  const {classes, nested, to, title, iconComponent, router, dispatch, ...subProps} = props;
  const IconComponent = iconComponent;
  const match = to !== undefined && router.location.pathname === to;
  return (
    <ListItem
      {...subProps}
      button
      component={to !== undefined ? Link : undefined}
      to={to}
      className={(nested ? classes.nested : '') + ' ' + (match ? classes.active : '')}
    >
      <ListItemIcon>
        <IconComponent/>
      </ListItemIcon>
      <ListItemText primary={title}/>
    </ListItem>
  );
};

DrawerListItem.propTypes = {
  nested: PropTypes.bool,
  iconComponent: PropTypes.func.isRequired,
  to: PropTypes.string,
  title: PropTypes.string.isRequired
};

export default connect(mapStateToProps)(withStyles(styles)(DrawerListItem));
