import React from 'react';

import {withStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import TrendingIcon from '@material-ui/icons/Whatshot';
import GenreIcon from '@material-ui/icons/QueueMusic';
import TimeIcon from '@material-ui/icons/AccessTime';
import RadioIcon from '@material-ui/icons/Radio';
import PlaylistIcon from '@material-ui/icons/QueueMusic';
import PlaylistItemIcon from '@material-ui/icons/PlaylistPlay';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import ArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import ListAltIcon from '@material-ui/icons/ListAlt';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import Divider from '@material-ui/core/Divider';
import Collapse from '@material-ui/core/Collapse';
import routes from '~/config/routes';
import {connect} from 'react-redux';
import DrawerListItem from "~/components/molecules/DrawerListItem";
import {translate} from 'react-i18next';
import * as tc from '~/i18n/constants';
import roles from '~/model/constants/roles';
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button/Button";

const mapStateToProps = state => ({
  auth: state.auth,
  drawer: state.drawer
});

const styles = (theme) => {
  console.log(theme);
  return {
    drawer: {
      position: 'relative',
      width: theme.mixins.drawer.width,
      height: 'calc(100vh - 64px)',
      marginTop: 64,
      left: -theme.mixins.drawer.width,
      [theme.breakpoints.up('md')]: {
        left:0,
      },
      transition: theme.transitions.create('left', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      })
    },
    drawerToggle: {
      left:0,
      [theme.breakpoints.up('md')]: {
        left: -theme.mixins.drawer.width,
      },
      transition: theme.transitions.create('left', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      })
    },
    nestedListItem: {
      paddingLeft: theme.spacing.unit * 6
    }
  }
};

class DrawerMenu extends React.Component {
  constructor(props) {
    super(props);

    console.log(props);

    this.state = {
      playlistMenuOpen: false
    };

    this.togglePlaylistMenu = this.togglePlaylistMenu.bind(this);
  }

  togglePlaylistMenu() {
    this.setState({
      playlistMenuOpen: !this.state.playlistMenuOpen
    });
  }

  render() {
    const {classes} = this.props;

    return (
      <nav>
        <Drawer
          //style={this.props.drawer.toggle ? {left: - this.props.theme.mixins.drawer.width} : undefined}
          variant="permanent"
          classes={{
            paper: classes.drawer + ' ' + (this.props.drawer.toggle ? classes.drawerToggle : '')
          }}>
          <List>
            <DrawerListItem title={this.props.t(tc.HOME)} iconComponent={HomeIcon} to={routes.root.path}/>
            <DrawerListItem title={this.props.t(tc.TRENDING)} iconComponent={TrendingIcon} to={routes.trends.path}/>
            <If condition={this.props.auth.isAuth}>
              <DrawerListItem title={this.props.t(tc.RECENTLY_PLAYED)} iconComponent={TimeIcon} to={routes.recent.path}/>
            </If>
            <DrawerListItem title={this.props.t(tc.GENRE)} iconComponent={GenreIcon} to={'/genre'}/>
            <DrawerListItem title={this.props.t(tc.RADIOS)} iconComponent={RadioIcon} to={'/radios'}/>
          </List>
          <If condition={this.props.auth.isAuth}>
            <Divider/>
            <List>
              <ListItem button onClick={this.togglePlaylistMenu}>
                <ListItemIcon>
                  <PlaylistIcon/>
                </ListItemIcon>
                <ListItemText primary={this.props.t(tc.PLAYLISTS)}/>
                {this.state.playlistMenuOpen ? <ArrowDownIcon/> : <ArrowRightIcon/>}
              </ListItem>
              <Collapse in={this.state.playlistMenuOpen} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  <DrawerListItem title='Favorites' iconComponent={FavoriteIcon} nested to={routes.favorites.path}/>
                  <For each="playlistName" index="idx" of={['Classique']}>
                    <DrawerListItem key={idx} title={playlistName} iconComponent={PlaylistItemIcon} to={`/genre/5`} nested/>
                  </For>
                  <DrawerListItem title='Add playlist' iconComponent={PlaylistAddIcon} nested/>
                </List>
              </Collapse>
            </List>
          </If>
          <If condition={this.props.auth.isAuth && (this.props.auth.role === roles.MODERATOR || this.props.auth.role === roles.ADMIN || this.props.auth.role === roles.OWNER)}>
            <Divider/>
            <List>
              <If condition={this.props.auth.role === roles.MODERATOR || this.props.auth.role === roles.ADMIN}>
                <DrawerListItem title={/*this.props.t(tc.RADIOS)*/'Moderation'} iconComponent={ListAltIcon} to={routes.moderation.path}/>
              </If>
              <If condition={this.props.auth.role === roles.OWNER || this.props.auth.role === roles.MODERATOR || this.props.auth.role === roles.ADMIN}>
                <DrawerListItem title={/*this.props.t(tc.RADIOS)*/'Gestion des musiques'} iconComponent={LibraryBooksIcon} to={routes.manage.path}/>
              </If>
            </List>
          </If>
        </Drawer>
      </nav>
    );
  }
}

export default connect(mapStateToProps)(translate()(withStyles(styles)(DrawerMenu)));
