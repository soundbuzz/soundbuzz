import React from 'react';

import {withStyles} from '@material-ui/core/styles';
import {connect} from 'react-redux';
import {translate} from 'react-i18next';
import * as tc from '~/i18n/constants';
import Typography from '@material-ui/core/Typography';
import PlayerProgressBar from "~/components/molecules/PlayerProgressBar";

import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import VolumeDownIcon from '@material-ui/icons/VolumeDown';
import VolumeMuteIcon from '@material-ui/icons/VolumeMute';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import ShuffleIcon from '@material-ui/icons/Shuffle';
import CommentIcon from '@material-ui/icons/Comment';
import IconButton from '@material-ui/core/IconButton';
import Popover from '@material-ui/core/Popover';
import Slider from '@material-ui/lab/Slider';
import {login as loginAction} from "~/model/actions/auth";
import {volumeChange, progressChange, togglePlay, setTimers, likeCurrentMusic, unlikeCurrentMusic, addComment} from "~/model/actions/player";
import AudioPlayer from "~/components/atoms/AudioPlayer";
import Modal from '@material-ui/core/Modal';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TimeIcon from "@material-ui/core/SvgIcon/SvgIcon";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const secondsToMinutes = function(sec){
  // Hours, minutes and seconds
  let hrs = ~~(sec / 3600);
  let mins = ~~((sec % 3600) / 60);
  let secs = sec % 60;

  // Output like "1:01" or "4:03:59" or "123:03:59"
  let ret = "";

  if (hrs > 0) {
    ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
  }

  ret += "" + mins + ":" + (secs < 10 ? "0" : "");
  ret += "" + secs;
  return ret;
};

const mapStateToProps = state => ({
  player: state.player,
  auth: state.auth
});

const styles = (theme) => {
  return {
    root: {
      display: 'flex',
      // flexDirection: 'row',
      // flexWrap: 'nowrap',

      height: theme.mixins.player.height,
      width: '100%',
      backgroundColor: theme.palette.background.default,
      // boxShadow: theme.shadows[4]
      //borderTop: '1px solid #cecece'
    },
    albumImageContainer: {
      display: 'flex',
      alignItems: 'center'
    },
    albumImage: {
      backgroundColor: '#e2e2e2',
      width: 30,
      height: 30,
      margin: '0 10px',
      overflow: 'hidden'
    },
    titlesContainer: {
      display: 'flex',
      alignItems: 'center',
      marginRight: 5
    },
    favoriteContainer: {
    },
    downloadContainer: {
    },
    currentTimeContainer: {
      display: 'flex',
      alignItems: 'center',
      marginLeft: 10
    },
    progressBarContainer: {
      flexGrow: 1,
      display: 'flex',
      alignItems: 'center',
      padding: '0 10px',
      cursor: 'pointer'
    },
    totalTimeContainer: {
      display: 'flex',
      alignItems: 'center',
      marginRight: 10
    },
    controls: {
      marginRight: 10
    },
    volumeSliderContainer: {
      height: 100,
      padding: '10px 0'
    },
    modalInner: {
      position: 'absolute',
      width: '80%',
      height: '80%',
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      overflowY: 'auto'
    },
    commentCard: {
      marginTop: 16,
      backgroundColor: theme.palette.primary.main
    }
  }
};

class Player extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      volumeMenuAnchor: null,
      commentsModalOpened: false
    };

    this.audioPlayerRef = React.createRef();
    this.commentInputRef = React.createRef();

    this.handleVolumeMenuOpen = this.handleVolumeMenuOpen.bind(this);
    this.handleVolumeMenuClose = this.handleVolumeMenuClose.bind(this);
    this.handleVolumeChange = this.handleVolumeChange.bind(this);
    this.handleProgressChange = this.handleProgressChange.bind(this);
    this.togglePlay = this.togglePlay.bind(this);
    this.onListen = this.onListen.bind(this);
    this.onLoadedMetadata = this.onLoadedMetadata.bind(this);
    this.onLike = this.onLike.bind(this);
    this.openModal = this.openModal.bind(this);
    this.onModalClose = this.onModalClose.bind(this);
    this.postComment = this.postComment.bind(this);
  }

  handleVolumeMenuOpen(e){
    this.setState({
      volumeMenuAnchor: e.currentTarget,
    });
  }

  handleVolumeMenuClose(){
    this.setState({
      volumeMenuAnchor: null,
    });
  }

  handleVolumeChange(e, value){
    this.props.dispatch(volumeChange(value));
  }

  handleProgressChange(e, value){
    //this.props.dispatch(progressChange(value));
    if(this.audioPlayerRef){
      this.audioPlayerRef.current.audioEl.currentTime = value;
      this.onListen(value)
    }
  }

  togglePlay(){
    this.props.dispatch(togglePlay());
  }

  onListen(sec){
    this.props.dispatch(progressChange(sec));
  }

  componentDidUpdate(prevProps, prevState, snapshot){
    if(this.audioPlayerRef){
      if(prevProps.player.play === false && this.props.player.play === true){
        this.audioPlayerRef.current.audioEl.play();
      }else if(prevProps.player.play === true && this.props.player.play === false){
        this.audioPlayerRef.current.audioEl.pause();
      }else if (prevProps.player.currentMusic &&
        prevProps.player.currentMusic.id !=
        this.props.player.currentMusic.id &&
        this.props.player.play){
        this.audioPlayerRef.current.audioEl.play();
      }
    }
  }

  onLoadedMetadata(){
    console.log('onLoadedMetadata');
    if(this.audioPlayerRef.current){
      this.props.dispatch(setTimers(this.audioPlayerRef.current.audioEl.duration));
    }
  }

  onLike(){
    console.log('onLike');
    if(this.props.player.currentMusic.liked){
      console.log('unlike');
      this.props.dispatch(unlikeCurrentMusic(this.props.player.currentMusic.likeId));
    }else{
      console.log('like');
      this.props.dispatch(likeCurrentMusic(this.props.auth.userId, this.props.player.currentMusic.id));
    }
  }

  onModalClose(){
    console.log('close modal');
    this.setState({
      commentsModalOpened: false
    });
  }

  openModal(){
    console.log('open modal');
    this.setState({
      commentsModalOpened: true
    });
  }

  postComment(){
    this.props.dispatch(addComment(this.props.auth.userId, this.props.auth.username, this.props.player.currentMusic.id, this.commentInputRef.current.value));
    this.commentInputRef.current.value = '';
  }

  render() {
    const {classes} = this.props;

    return (
      <div className={classes.root}>
        <AudioPlayer
          src={this.props.player.currentMusic ? this.props.player.currentMusic.audioFile : ''}
          ref={this.audioPlayerRef}
          onListen={this.onListen}
          volume={this.props.player.volume / 100}
          progress={this.props.player.currentProgress}
          onLoadedMetadata={this.onLoadedMetadata}
        />
        <div className={classes.albumImageContainer}>
          <div className={classes.albumImage}>
            <If condition={this.props.player.currentMusic && this.props.player.currentMusic.picture}>
              <img style={{width: '100%', height: '100%'}} src={this.props.player.currentMusic.picture}/>
            </If>
          </div>
        </div>
        <div className={classes.titlesContainer}>
          <div>
            <Typography variant='caption' style={{fontWeight: 'bold'}}>{this.props.player.currentMusic ? this.props.player.currentMusic.artist : '--------'}</Typography>
            <Typography variant='caption'>{this.props.player.currentMusic ? this.props.player.currentMusic.title : '------'}</Typography>
          </div>
        </div>
        <If condition={this.props.auth.isAuth}>
          <div className={classes.favoriteContainer}>
            <IconButton color='primary' component="span" onClick={this.onLike} disabled={this.props.player.isWebRadio}>
              <Choose>
                <When condition={this.props.player.currentMusic && this.props.player.currentMusic.liked}>
                  <FavoriteIcon/>
                </When>
                <Otherwise>
                  <FavoriteBorderIcon/>
                </Otherwise>
              </Choose>
            </IconButton>
          </div>
        </If>
        <IconButton color='primary' onClick={this.openModal} disabled={this.props.player.currentMusic == null || this.props.player.isWebRadio}>
          <CommentIcon/>
        </IconButton>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.commentsModalOpened}
          onClose={this.onModalClose}
        >
          <div className={this.props.classes.modalInner}>
            <If condition={this.props.player.currentMusic && this.props.player.currentMusic.comment}>
              <Typography variant="title" id="modal-title" color="textSecondary">
                {/*Liste des commentaires pour {this.props.player.currentMusic.title} de {this.props.player.currentMusic.artist}*/}
                {this.props.player.currentMusic.title} : commentaires
              </Typography>
              <For each="comment" index="idx" of={this.props.player.currentMusic.comment}>
                <Card className={this.props.classes.commentCard} color="primary" key={idx}>
                  <CardContent style={{paddingBottom: 16}}>
                    <Typography key={idx} variant="subheading" id="simple-modal-description" style={{color: '#fff'}}>
                      {comment.user_id.login} : {comment.content}
                    </Typography>
                  </CardContent>
                </Card>
              </For>
            </If>
            <If condition={this.props.auth.isAuth}>
              <TextField
                id="outlined-textarea"
                label="Ajouter un commentaire"
                placeholder="Commentaire ..."
                multiline
                margin="normal"
                variant="outlined"
                fullWidth
                inputRef={this.commentInputRef}
              />
              <Button variant="contained" color="primary" fullWidth style={{marginTop: 16}} onClick={this.postComment}>
                Ajouter
              </Button>
            </If>
            <div style={{width: 30, display: 'inline-block', marginTop: 16, marginRight: 16, color: '#512da8'}}>
              <svg aria-hidden="true" data-prefix="fab" data-icon="facebook" role="img" xmlns="http://www.w3.org/2000/svg"
                   viewBox="0 0 448 512" className="svg-inline--fa fa-facebook fa-w-14 fa-2x">
                <path fill="currentColor"
                      d="M448 56.7v398.5c0 13.7-11.1 24.7-24.7 24.7H309.1V306.5h58.2l8.7-67.6h-67v-43.2c0-19.6 5.4-32.9 33.5-32.9h35.8v-60.5c-6.2-.8-27.4-2.7-52.2-2.7-51.6 0-87 31.5-87 89.4v49.9h-58.4v67.6h58.4V480H24.7C11.1 480 0 468.9 0 455.3V56.7C0 43.1 11.1 32 24.7 32h398.5c13.7 0 24.8 11.1 24.8 24.7z"
                      className=""></path>
              </svg>
            </div>
            <div style={{width: 30, display: 'inline-block', marginTop: 16, color: '#512da8'}}>
              <svg aria-hidden="true" data-prefix="fab" data-icon="twitter-square" role="img"
                   xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"
                   className="svg-inline--fa fa-twitter-square fa-w-14 fa-2x">
                <path fill="currentColor"
                      d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-48.9 158.8c.2 2.8.2 5.7.2 8.5 0 86.7-66 186.6-186.6 186.6-37.2 0-71.7-10.8-100.7-29.4 5.3.6 10.4.8 15.8.8 30.7 0 58.9-10.4 81.4-28-28.8-.6-53-19.5-61.3-45.5 10.1 1.5 19.2 1.5 29.6-1.2-30-6.1-52.5-32.5-52.5-64.4v-.8c8.7 4.9 18.9 7.9 29.6 8.3a65.447 65.447 0 0 1-29.2-54.6c0-12.2 3.2-23.4 8.9-33.1 32.3 39.8 80.8 65.8 135.2 68.6-9.3-44.5 24-80.6 64-80.6 18.9 0 35.9 7.9 47.9 20.7 14.8-2.8 29-8.3 41.6-15.8-4.9 15.2-15.2 28-28.8 36.1 13.2-1.4 26-5.1 37.8-10.2-8.9 13.1-20.1 24.7-32.9 34z"
                      className=""></path>
              </svg>
            </div>
          </div>
        </Modal>
        <If condition={this.props.player.currentMusic && this.props.player.currentMusic.authDownload}>
          <div className={classes.downloadContainer}>
            <IconButton disabled={this.props.player.currentMusic == null} href={this.props.player.currentMusic ? this.props.player.currentMusic.audioFile : null} download color='primary' component="a">
              <SaveAltIcon/>
            </IconButton>
          </div>
        </If>
        <div className={classes.currentTimeContainer}>
          <Typography>{this.props.player.currentProgress !== null ? secondsToMinutes(Math.round(this.props.player.currentProgress)) : '-:--'}</Typography>
        </div>
        <div className={classes.progressBarContainer}>
          {/*<PlayerProgressBar value={20}/>*/}
          <Slider disabled={this.props.player.currentMusic == null || this.props.player.isWebRadio} min={0} max={this.props.player.totalTime != null ? Math.round(this.props.player.totalTime) : 0} value={this.props.player.currentProgress ? this.props.player.currentProgress : 0} onChange={this.handleProgressChange}/>
        </div>
        <div className={classes.totalTimeContainer}>
          <Typography>{this.props.player.totalTime != null && !this.props.player.isWebRadio ? secondsToMinutes(Math.round(this.props.player.totalTime)) : '-:--'}</Typography>
        </div>
        <div className={classes.controls}>
          <IconButton color='primary' component="span" disabled={this.props.player.currentMusic == null || this.props.player.isWebRadio || this.props.player.queue.prev.length <= 0}>
            <ShuffleIcon/>
          </IconButton>
          <IconButton color='primary' component="span" disabled={this.props.player.queue.prev.length <= 0}>
            <SkipPreviousIcon/>
          </IconButton>
          <IconButton color='primary' component="span" onClick={this.togglePlay} disabled={this.props.player.currentMusic == null}>
            <Choose>
              <When condition={this.props.player.play === true}>
                <PauseIcon/>
              </When>
              <Otherwise>
                <PlayArrowIcon/>
              </Otherwise>
            </Choose>
          </IconButton>
          <IconButton color='primary' component="span" disabled={this.props.player.queue.next.length <= 0}>
            <SkipNextIcon/>
          </IconButton>
          <IconButton color='primary' component="span" onClick={this.handleVolumeMenuOpen}>
            <Choose>
              <When condition={this.props.player.volume > 50}>
                <VolumeUpIcon/>
              </When>
              <When condition={this.props.player.volume > 0}>
                <VolumeDownIcon/>
              </When>
              <Otherwise>
                <VolumeMuteIcon/>
              </Otherwise>
            </Choose>
          </IconButton>
          <Popover
            classes={{paper: classes.volumeSliderContainer}}
            id="volume-popper"
            open={this.state.volumeMenuAnchor !== null}
            anchorEl={this.state.volumeMenuAnchor}
            onClose={this.handleVolumeMenuClose}
            elevation={4}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
          >
            <Slider value={this.props.player.volume} vertical reverse onChange={this.handleVolumeChange}/>
          </Popover>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(translate()(withStyles(styles)(Player)));
