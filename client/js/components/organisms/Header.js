import React from 'react';
import {connect} from 'react-redux';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import NotificationIcon from '@material-ui/icons/Notifications';
import UploadIcon from '@material-ui/icons/CloudUpload';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';
import routes from '~/config/routes';
import AppSearch from '~/components/molecules/AppSearchInput';
import {fade} from "@material-ui/core/styles/colorManipulator";
import {Link} from "react-router-dom";
import {logout} from "~/model/actions/auth";
import {toggle as drawerToggle} from "~/model/actions/drawer";
import Tooltip from '@material-ui/core/Tooltip';

const mapStateToProps = state => ({
  auth: state.auth
});

const styles = (theme) => ({
  root: {
    zIndex: theme.zIndex.drawer + 1
  },
  title: {
    marginLeft: theme.spacing.unit,
  },
  titleLink: {
    color: 'inherit',
    textDecoration: 'inherit',
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'block'
    }
  },
  search: {
    flexGrow: 1,
    marginLeft: 0,
    [theme.breakpoints.up('md')]: {
      marginLeft: theme.spacing.unit * 4
    }
    // position: 'absolute',
    // width: '50%',
    // left: 0,
    // right: 0,
    // margin: 'auto'
  },
  authButton: {
    color: 'inherit',
    textDecoration: 'inherit'
  },
  inlineWrapper: {
    display: 'inline-block'
  },
  username: {
    '&:focus':{
      outline: 'none'
    }
  },
  parametersLink: {
    color: 'rgba(0, 0, 0, 0.87)',
    textDecoration: 'none'
  }
});

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userMenuOpen: false,
      userMenuButtonEl: null,
      notificationsMenuOpen: false,
      notificationsMenuButtonEl: null
    };

    this.openUserMenu = this.openUserMenu.bind(this);
    this.closeUserMenu = this.closeUserMenu.bind(this);
    this.openNotificationsMenu = this.openNotificationsMenu.bind(this);
    this.closeNotificationsMenu = this.closeNotificationsMenu.bind(this);
    this.logout = this.logout.bind(this);
    this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
  }

  openUserMenu(event){
    this.setState({
      userMenuOpen: true,
      userMenuButtonEl: event.currentTarget
    });
  }

  closeUserMenu(){
    this.setState({
      userMenuOpen: false,
      userMenuButtonEl: null
    });
  }

  openNotificationsMenu(event){
    this.setState({
      notificationsMenuOpen: true,
      notificationsMenuButtonEl: event.currentTarget
    });
  }

  closeNotificationsMenu(){
    this.setState({
      notificationsMenuOpen: false,
      notificationsMenuButtonEl: null
    });
  }

  logout(){
    this.props.dispatch(logout());
  }

  handleDrawerToggle(){
    this.props.dispatch(drawerToggle());
  }

  render() {
    const {classes} = this.props;

    return (
      <AppBar position="absolute" className={classes.root}>
        <Toolbar>
          <IconButton color="inherit" aria-label="Menu" onClick={this.handleDrawerToggle}>
            <MenuIcon />
          </IconButton>
          <Typography variant="title" color="inherit" className={classes.title}>
            <Link to={routes.root.path} className={classes.titleLink}>
              Soundbuzz
            </Link>
          </Typography>
          <AppSearch className={classes.search}/>
          <Choose>
            <When condition={this.props.auth.isAuth}>
              <div>
                <div className={classes.inlineWrapper}>
                  <Tooltip title="Upload new music">
                    <Link to={routes.upload.path} className={classes.authButton}>
                      <IconButton
                        color="inherit"
                      >
                        <UploadIcon />
                      </IconButton>
                    </Link>
                  </Tooltip>
                </div>
                <div className={classes.inlineWrapper}>
                  <IconButton
                    aria-owns={this.state.notificationsMenuOpen ? 'notifications-menu' : null}
                    aria-haspopup="true"
                    onClick={this.openNotificationsMenu}
                    color="inherit"
                  >
                    {/*TODO: if notifications: notification_active icon*/}
                    <NotificationIcon />
                  </IconButton>
                  <Menu
                    id="notifications-menu"
                    anchorEl={this.state.notificationsMenuButtonEl}
                    getContentAnchorEl={null}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'center',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'center',
                    }}
                    open={this.state.notificationsMenuOpen}
                    onClose={this.closeNotificationsMenu}
                  >
                    <MenuItem>There are no notifications</MenuItem>
                  </Menu>
                </div>
                <div className={classes.inlineWrapper}>
                  <IconButton
                    aria-owns={this.state.userMenuOpen ? 'user-menu' : null}
                    aria-haspopup="true"
                    onClick={this.openUserMenu}
                    color="inherit"
                  >
                    <AccountCircleIcon />
                  </IconButton>
                  <Menu
                    id="user-menu"
                    anchorEl={this.state.userMenuButtonEl}
                    getContentAnchorEl={null}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'center',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'center',
                    }}
                    open={this.state.userMenuOpen}
                    onClose={this.closeUserMenu}
                  >
                    <li className={this.props.classes.username}>
                      <Typography align="center" variant="subheading" color="textSecondary" style={{margin: '12px 0'}}>
                        {this.props.auth.username}
                      </Typography>
                    </li>
                    <MenuItem style={{padding: '12px 0'}}>
                      <Link
                        className={this.props.classes.parametersLink}
                        to={routes.settings.path}
                        style={{display: 'block', padding: '0 16px'}}
                      >
                        Paramètres
                      </Link>
                    </MenuItem>
                    <MenuItem onClick={() => {this.closeUserMenu(); this.logout();}}>Logout</MenuItem>
                  </Menu>
                </div>
              </div>
            </When>
            <Otherwise>
              <Link key={0} to={routes.login.path} className={classes.authButton}>
                <Button color="inherit">Login</Button>
              </Link>
              <Link key={1} to={routes.signup.path} className={classes.authButton}>
                <Button color="inherit">Sign up</Button>
              </Link>
            </Otherwise>
          </Choose>
        </Toolbar>
      </AppBar>
    );
  }
}

export default connect(mapStateToProps)(withStyles(styles)(Header));
