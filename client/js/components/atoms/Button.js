import MaterialButton from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import React from "react";
import {withStyles} from "@material-ui/core/styles";

const styles = (theme) => ({
  buttonWrapper: {
    display: 'inline-flex',
    //margin: theme.spacing.unit,
    position: 'relative',
  },
  button: {
    //width: '100%'
  },
  label: {
    opacity: 0.5
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
});

const Button = props => {
  const {classes, loading, ...subProps} = props;
  return (
    <div className={classes.buttonWrapper}>
      <MaterialButton
        {...subProps}
        className={classes.button + ' ' + subProps.className}
        disabled={subProps.disabled || loading}
        classes={
          subProps.disabled || loading ? {
            label: classes.label
          } : undefined
        }
      >
        {subProps.children}
      </MaterialButton>
      {loading && <CircularProgress size={24} className={classes.buttonProgress}/>}
    </div>
  );
};

Button.propTypes = {
  loading: PropTypes.bool
};

export default withStyles(styles)(Button);
