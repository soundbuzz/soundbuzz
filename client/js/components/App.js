import React from 'react';
import { TransitionGroup, CSSTransition } from "react-transition-group";
import {hot} from 'react-hot-loader';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {ConnectedRouter} from 'connected-react-router';
import {store, history} from '~/model/store';
import { Provider } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import _ from 'lodash';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import routes from '~/config/routes';
import MainLayout from '~/components/layouts/MainLayout';
import purple from '@material-ui/core/colors/deepPurple';
import { I18nextProvider } from 'react-i18next';
import i18nextInstance from '~/i18n/i18nextInstance';

const theme = createMuiTheme({
  palette: {
    primary: purple,
  },
  mixins: {
    drawer: {
      width: 260
    },
    searchInput: {
      width: 400
    },
    player: {
      height: 49
    }
  }
});

class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <CssBaseline/>
        <I18nextProvider i18n={i18nextInstance}>
          <MuiThemeProvider theme={theme}>
            <Provider store={store}>
              <ConnectedRouter history={history}>
                <Route
                  render={({ location }) => (
                    <MainLayout location={location}>
                      <Switch location={location}>
                        { _.map(routes, (route, key) => {
                          const isUserLogged = store.getState().auth.isAuth;
                          const {path, isPrivate} = route;
                          const Page = route.page;
                          return (
                            <Route
                              exact
                              path={path}
                              key={key}
                              render={(route) => {
                                  if(isPrivate){
                                    if(isUserLogged){
                                      return (
                                        <Page/>
                                      );
                                    }else{
                                      //TODO: redirect to login page
                                      return (
                                        <div>
                                          Your are not allowed to show this page ({route.location.pathname})
                                        </div>
                                      );
                                    }
                                  }else{
                                    return (
                                      <Page/>
                                    );
                                  }
                                }
                              }
                            />
                          )
                        })}
                      </Switch>
                    </MainLayout>
                  )}
                />
              </ConnectedRouter>
            </Provider>
          </MuiThemeProvider>
        </I18nextProvider>
      </React.Fragment>
    );
  }
}

export default hot(module)(App);
