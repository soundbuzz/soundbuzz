import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Header from '~/components/organisms/Header';
import DrawerMenu from '~/components/organisms/DrawerMenu';
import Player from '~/components/organisms/Player';
import Grid from "@material-ui/core/Grid";
import {CSSTransition, TransitionGroup} from "react-transition-group";
import '~/components/transitions.css';
import {connect} from 'react-redux';
import {toggle as drawerToggle} from "~/model/actions/drawer";

const mapStateToProps = state => ({
  drawer: state.drawer
});

const styles = (theme) => ({
  container: {
    overflow: 'hidden'
  },
  main: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100vh',
    width: '100vw',
    paddingTop: 64,
    overflow: 'hidden',

    paddingLeft: 0,
    transition: theme.transitions.create('padding-left', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    [theme.breakpoints.up('md')]: {
      paddingLeft: theme.mixins.drawer.width,
      transition: theme.transitions.create('padding-left', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      })
    },


  },
  transitionGroup: {
    height: `calc(100% - ${theme.mixins.player.height}px)`
  },
  drawerToggle: {
    paddingLeft: 0,
    // paddingLeft: theme.mixins.drawer.width,
    // [theme.breakpoints.up('md')]: {
    //   paddingLeft: 0,
    //
    // },
    transition: theme.transitions.create('padding-left', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    [theme.breakpoints.up('md')]: {
      transition: theme.transitions.create('padding-left', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      })
    },
  },
  grid: {
    margin: 0,
    flexGrow: 1,
    padding: theme.spacing.unit * 2,
    width: 'initial',
    height: '100%',
    overflow: 'auto'
  },
  overlay: {
    display: 'block',
    [theme.breakpoints.up('md')]: {
      display: 'none'
    },
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'black',
    pointerEvents: 'none',
    opacity: 0,
    zIndex: theme.zIndex.drawer - 1,
    transition: theme.transitions.create('opacity', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    })
  },
  overlayToggle: {
    opacity: .5,
    pointerEvents: 'auto',
    transition: theme.transitions.create('opacity', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    })
  },
  playerContainer: {
    bottom: 0,
    height: theme.mixins.player.height,
    width: '100%',
    backgroundColor: 'white',
    boxShadow: theme.shadows[4],
    zIndex: theme.zIndex.drawer - 2,
    position: 'relative'
  }
});

class MainLayout extends React.Component {
  constructor(props) {
    super(props);

    this.mustTransition = true;

    this.onOverlayClick = this.onOverlayClick.bind(this);
  }

  onOverlayClick(){
    this.props.dispatch(drawerToggle());
  }

  componentWillReceiveProps(nextProps, nextContext) {
    // Prevent page re-rendering (and animation triggering) when change location as same as previous
    this.mustTransition = nextProps.location.pathname !== this.props.location.pathname;
  }

  render() {
    const {classes} = this.props;
    return (
      <div className={classes.container}>
        <div
          className={classes.overlay + ' ' + (this.props.drawer.toggle ? classes.overlayToggle : '')}
          onClick={this.onOverlayClick}
        />
        <Header/>
        <DrawerMenu/>
        <main className={classes.main + ' ' + (this.props.drawer.toggle ? classes.drawerToggle : '')}>
          <TransitionGroup
            className={classes.transitionGroup}
            enter={this.mustTransition}
            childFactory={child => React.cloneElement(
              child,
              {classNames: this.mustTransition ? 'layout-fade'  : 'no-transition', timeout: this.mustTransition ? 200 : 0}
            )}
          >
            <CSSTransition key={this.props.location.key} classNames="layout-fade" timeout={200}>
                <Grid container spacing={24} className={classes.grid}>
                  {this.props.children}
                </Grid>
            </CSSTransition>
          </TransitionGroup>
          <div className={classes.playerContainer}>
            <Player/>
          </div>
        </main>
      </div>
    );
  }
}

export default connect(mapStateToProps)(withStyles(styles)(MainLayout));
