export const SEARCH_VOID = 'SEARCH_VOID'; // when no research
export const SEARCH_START = 'SEARCH_START'; // when search API call start
export const SEARCH_SUCCESS = 'SEARCH_SUCCESS'; // when search API call finish with success
export const SEARCH_ERROR = 'SEARCH_ERROR'; // when search API call finish with error
