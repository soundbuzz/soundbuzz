export default {
  USER: 'USER', // 0
  MODERATOR: 'MODERATOR', // 1
  OWNER: 'OWNER', // 2
  ADMIN: 'ADMIN' // 3
};
