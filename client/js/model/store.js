import {createStore, applyMiddleware, compose} from 'redux';
import {connectRouter, routerMiddleware} from 'connected-react-router'
import reduxThunk from 'redux-thunk';
import reducer from '~/model/reducer';
import {createBrowserHistory} from 'history';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createBrowserHistory();

export const store =  createStore(connectRouter(history)(reducer), composeEnhancer(applyMiddleware(reduxThunk, routerMiddleware(history))));
