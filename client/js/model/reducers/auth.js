import {LOGIN, LOGOUT, AUTH_START, AUTH_FAIL, SET_OWNER} from '~/model/constants/authActionTypes';
import roles from '~/model/constants/roles';

let rememberAuth = localStorage.getItem('remember_auth');

let initialState;

if(rememberAuth == null){
  initialState = {
    pending: false,
    isAuth: false,
    token: null,
    fail: false,

    userId: null,
    username: null,
    role: null
  };
}else{
  rememberAuth = JSON.parse(rememberAuth);
  initialState = {
    pending: false,
    isAuth: true,
    token: null,
    fail: false,

    userId: rememberAuth.userId,
    username: rememberAuth.username,
    role: rememberAuth.role
  };
}


export default (state = initialState, action) => {
  switch (action.type) {
    case SET_OWNER:
      return {
        ...state,
        role: state.role === roles.USER ? roles.OWNER : state.role
      };
    case AUTH_START:
      return {
        ...state,
        fail: false,
        pending: true
      };
    case AUTH_FAIL:
      return {
        ...state,
        fail: true,
        pending: false
      };
    case LOGIN:
      console.log('remember:', action.remember);
      if(action.remember === true){
        localStorage.setItem('remember_auth', JSON.stringify({
          username: action.username,
          role: action.role,
          userId: action.userId
        }));
      }
      return {
        ...state,
        fail: false,
        pending: false,
        isAuth: true,
        username: action.username,
        role: action.role,
        userId: action.userId
      };
    case LOGOUT:
      localStorage.removeItem('remember_auth');
      return {
        pending: false,
        isAuth: false,
        token: null,
        fail: false,

        userId: null,
        username: null,
        role: null
      };
    default:
      return state;
  }
};
