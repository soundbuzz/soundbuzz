import {SEARCH_START, SEARCH_SUCCESS, SEARCH_ERROR, SEARCH_VOID} from '~/model/constants/searchActionTypes';

const initialState = {
  pending: false, // when wait for API response
  results: null, // the results/response of search API, null if no search, [] if search without results
  error: false,
  isSearching: false,
  timestamp: null // last search timestamp
};

// if it's not the last search fired
function actionIsValid(state, timestamp) {
  return timestamp >= state.timestamp;
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_START:
      return {
        ...state,
        results: null,
        pending: true,
        error: false,
        isSearching: true,
        timestamp: action.timestamp
      };
    case SEARCH_SUCCESS:
      if(!actionIsValid(state, action.timestamp)){
        return state;
      }
      return {
        ...state,
        results: action.results,
        pending: false,
        error: false,
        isSearching: false
      };
    case SEARCH_ERROR:
      if(!actionIsValid(state, action.timestamp)){
        return state;
      }
      return {
        ...state,
        results: null,
        pending: false,
        error: true,
        voidSearch: false
      };
    case SEARCH_VOID:
      return initialState;
    default:
      return state;
  }
};
