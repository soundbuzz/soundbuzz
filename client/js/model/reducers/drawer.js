import {TOGGLE} from '~/model/constants/drawerActionTypes';

const initialState = {
  toggle: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE:
      return {
        toggle: !state.toggle
      };
    default:
      return state;
  }
}
