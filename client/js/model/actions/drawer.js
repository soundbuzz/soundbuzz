import {TOGGLE} from '~/model/constants/drawerActionTypes';

export const toggle = () => ({
  type: TOGGLE
});
