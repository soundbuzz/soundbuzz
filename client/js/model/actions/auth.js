import {push} from 'connected-react-router'
import routes from '~/config/routes';
import {LOGIN, AUTH_START, LOGOUT, AUTH_FAIL, SET_OWNER} from '~/model/constants/authActionTypes';
import env from '~/config/env';
import roles from '~/model/constants/roles';

// export const login = (username) => ({
//   type: LOGIN,
//   username
// });

// async action
export const login = (username, password, remember) => dispatch => {
  /**
   * Pending auth
   */
  dispatch({
    type: AUTH_START
  });


  fetch(`${env.API_BASE_URL}api/v1/auth/login`, {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      "login": username,
      "password": password
    })
  }).then(function (response) {
    if(response.status !== 200){
      console.log('Auth fail');
      dispatch({
        type: AUTH_FAIL
      });
    }else{
      console.log('Auth success');
      console.log(response);
      return response.json();
    }
  })
  .then(function (user) {
    // if auth success
    if(user){
      console.log(user);
      let role = roles.USER;
      
      switch (user.role) {
        case 1:
          role = roles.OWNER;
          break;
        case 2:
          role = roles.MODERATOR;
          break;
        case 3:
          role = roles.ADMIN;
          break;
      }

      dispatch({
        type: LOGIN,
        userId: user.id,
        username: user.login,
        role: role,
        remember: remember
      });

      dispatch(push(routes.root.path));
    }
  });
};

// set user as owner if user is role user
export const setOwner = () => ({
  type: SET_OWNER
});

export const logout = () => dispatch => {
  dispatch({
    type: LOGOUT
  });

  dispatch(push(routes.root.path));
};
