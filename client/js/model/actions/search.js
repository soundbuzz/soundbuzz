import {SEARCH_START, SEARCH_SUCCESS, SEARCH_ERROR, SEARCH_VOID} from '~/model/constants/searchActionTypes';
import {push} from "connected-react-router";
import routes from "~/config/routes";
import env from "~/config/env";

let previousSearchTimeout = null;

export const search = (value) => dispatch => {
  if(!value || value.length <= 3){
    dispatch({
      type: SEARCH_VOID
    });
    return;
  }

  const timestamp = Date.now();

  /**
   * Pending search
   */
  dispatch({
    type: SEARCH_START,
    timestamp
  });

  // if(previousSearchTimeout){
  //   clearTimeout(previousSearchTimeout);
  // }

  fetch(`${env.API_BASE_URL}sound`, {
    method: "GET",
    headers: {
      'Accept': 'application/json'
    }
  }).then(response => {
    return response.json();
  }).then(tracks => {
    console.log('api list of tracks');
    console.log(tracks);
    let searchMatchList = [];

    tracks = tracks.filter(track => {
      return track.actives;
    });

    tracks.forEach(track => {
      if(track.title.toLowerCase().indexOf(value.toLowerCase()) >= 0 || track.artist.toLowerCase().indexOf(value.toLowerCase()) >= 0){
        searchMatchList.push(track);
      }
    });

    dispatch({
      type: SEARCH_SUCCESS,
      results: searchMatchList,
      timestamp
    });
  });
};
