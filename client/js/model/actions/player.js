import {PLAY, PAUSE, STOP, VOLUME_CHANGE, PROGRESS_CHANGE, TOGGLE_PLAY, SET_TIMERS, SET_CURRENT_MUSIC, LIKE_CURRENT_MUSIC, UNLIKE_CURRENT_MUSIC, ADD_COMMENT, PLAY_WEBRADIO} from '~/model/constants/playerActionTypes';
import env from '~/config/env';
import {AUTH_FAIL, LOGOUT} from "~/model/constants/authActionTypes";
import {push} from "connected-react-router";
import routes from "~/config/routes";

export const volumeChange = (value) => {
  localStorage.setItem('volume', value);

  return {
    type: VOLUME_CHANGE,
    value
  };
};

export const progressChange = (value) => {
  return {
    type: PROGRESS_CHANGE,
    value
  };
};

export const play = () => {
  return {
    type: PLAY
  };
};

export const togglePlay = () => {
  return {
    type: TOGGLE_PLAY
  };
};


export const setTimers = (totalTime) => {
  return {
    type: SET_TIMERS,
    currentProgress: 0,
    totalTime
  };
};

export const setCurrentMusic = (id, userId) => dispatch => {
  let today = new Date();
  let minute = today.getMinutes();
  let hour = today.getHours();
  let dd = today.getDate();
  let mm = today.getMonth()+1; //January is 0!
  let yyyy = today.getFullYear();


  let recents = localStorage.getItem('recents') || "[]";
  recents = JSON.parse(recents);
  recents.push({
    id,
    date: `${dd}/${mm}/${yyyy} à ${hour}h${minute}`
  });
  localStorage.setItem('recents', JSON.stringify(recents));

  fetch(`${env.API_BASE_URL}sound/${id}`, {
    method: "GET",
    headers: {
      'Accept': 'application/json'
    }
  }).then(response => {
    return response.json();
  }).then(track => {
    console.log(track);
    let isLiked = false;
    let likeId = null;

    track.like.forEach((l) => {
      if(l.user_id == userId){
        isLiked = true;
        likeId = l.id;
      }
    });

    fetch(`${env.API_BASE_URL}comment?where={"sound_id":${track.id}}`, {
      method: "GET",
      headers: {
        'Accept': 'application/json'
      }
    }).then(response => {
      return response.json();
    }).then(comment => {
      dispatch({
        type: SET_CURRENT_MUSIC,
        id: track.id,
        title: track.title,
        artist: track.artist,
        picture: track.photo,
        liked: isLiked,
        likeId,
        comment: comment,
        authDownload: track.authorization_download == 'true'
      });
    });
  });
};

export const likeCurrentMusic = (userId, soundId) => dispatch => {
  fetch(`${env.API_BASE_URL}like`, {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      'user_id': userId,
      'sound_id': soundId
    })
  }).then(response => {

    return response.json()
  }).then(l => {
    console.log(l);
    dispatch({
      type: LIKE_CURRENT_MUSIC,
      likeId: l.id
    });
  });
};

export const unlikeCurrentMusic = (likeid) => dispatch => {
  fetch(`${env.API_BASE_URL}like/${likeid}`, {
    method: "DELETE",
    headers: {
    }
  }).then(response => {
    console.log(response);
    dispatch({
      type: UNLIKE_CURRENT_MUSIC,
    });
  });
};

export const addComment = (userId, username, soundId, comment) => dispatch => {
  fetch(`${env.API_BASE_URL}comment`, {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      'content': comment,
      'user_id': userId,
      'sound_id': soundId
    })
  }).then(response => {
    console.log(response);
    dispatch({
      type: ADD_COMMENT,
      username: username,
      content: comment
    });
  });
};

export const playWebradio = () => {
  return {
    type: PLAY_WEBRADIO,
  };
};
