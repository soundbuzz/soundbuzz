import {combineReducers} from 'redux';
import auth from '~/model/reducers/auth';
import search from '~/model/reducers/search';
import drawer from '~/model/reducers/drawer';
import player from '~/model/reducers/player';

/**
 * Global app reducer that combine all sub reducers
 */
export default combineReducers({
  auth,
  search,
  drawer,
  player
});
