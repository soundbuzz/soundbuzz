/**
 * ChangepasswordController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

 module.exports = {
   changepassword: async function (req, res) {
     if (req.param('new_password') != undefined && req.param('id') != undefined){
     await User.update({id:req.param('id')})
     .set({password: await sails.helpers.passwords.hashPassword(req.param('new_password')),
});

     console.log('Updated password in user id: '+req.param('id'));
     return res.ok();
   }
   else {
     res.json({succes:false, message: 'param is empty'});
   }
     }

 };
