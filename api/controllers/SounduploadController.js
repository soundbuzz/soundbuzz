/**
 * SounduploadController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
let dotenv = require('dotenv');
dotenv.config();

module.exports = {
    upload: async function (req, res) {
      console.log(req.param('id'));
        req.file('sound_file').upload({
            dirname: require('path').resolve(sails.config.appPath, process.env.PATH_SOUND_FILE),
            saveAs: req.param('id')
          },function (err, uploadedFiles) {
            if (err) return res.serverError(err);

            return res.json({
              sucess: true
            });
          });
      }

};
