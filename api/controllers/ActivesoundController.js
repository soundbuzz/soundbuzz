/**
 * ActivesoundController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  active: async function (req, res) {
    if (req.param('id') != undefined && req.param('value') != undefined && typeof(req.param('value')) === "boolean"){
    await Sound.update({id:req.param('id')})
    .set({actives:req.param('value')});

    console.log('Updated active in sound with id: '+req.param('id')+ 'and value active column: '+req.param('value'));
    return res.ok();
  }
  else {
    res.json({succes:false, message: 'param is empty'});
  }
    }

};
