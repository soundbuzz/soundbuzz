/**
 * MailconfirmController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  mailconfirm: async function (req, res) {
    if (req.param('token') != undefined && req.param('id') != undefined){
    await Sound.update({id:req.param('value')})
    .set({actives:req.param('value')});

    console.log('Updated active in sound with id: '+req.param('id'));
    return res.ok();
  }
  else {
    res.json({succes:false, message: 'param is empty'});
  }
    }

};
