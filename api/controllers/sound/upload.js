/**
 * TestController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    friendlyName: 'Sound',


    description: 'add sound',


    inputs: {
      title: {
        required: true,
        type: 'string',
      },

      date: {
        required: false,
        type: 'string',
      },

      description:  {
        required: false,
        type: 'string',
      },

      photo:  {
        required: false,
        type: 'string',
      },

      artist:  {
        required: false,
        type: 'string',
      },

      song_writer:  {
        required: false,
        type: 'string',
      },

      explicit_content:  {
        required: false,
        type: 'boolean',
      },

      authorization_download:  {
        required: false,
        type: 'string',
      },

      create_date:  {
        required: false,
        type: 'string',
      },

      duration_sound:  {
        required: false,
        type: 'string',
      },

      user_id:  {
        required: true,
        type: 'number',
      },

      genre_id:  {
        required: true,
        type: 'number',
      },

    },


    exits: {

    },


    fn: async function (inputs, exits) {
      var newSoundRecord = await Sound.create(Object.assign({
        title: inputs.title,
        date: inputs.date,
        description: inputs.description,
        photo: inputs.photo,
        artist : inputs.artist,
        song_writer : inputs.song_writer,
        explicit_content: inputs.explicit_content,
        authorization_download: inputs.authorization_download,
        create_date: inputs.create_date,
        duration_sound: inputs.duration_sound,
        user_id: inputs.user_id,
        genre_id: inputs.genre_id,
      }, ))
      .fetch();

    if (newSoundRecord){
        await User.update({id:inputs.user_id, role:0})
        .set({role:1});
        return exits.success(newSoundRecord);
    }
    else {
        return exits.success({"status":false})
    }

    }


};
