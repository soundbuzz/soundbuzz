/**
 * TestController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    friendlyName: 'Sound',


    description: 'delete sound',


    inputs: {
      id:  {
        required: true,
        type: 'number',
      },

    },


    exits: {

    },


    fn: async function (inputs, exits) {
      deleteSoundRecord = await Sound.destroy({id:inputs.id});
        return exits.success('OK')


    }


};
