/**
 * SounddownloadController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    friendlyName: 'sound',
    description: 'upload and download a sound file.',
  
    download: async function (req,res) {
        var fs = require('fs');
        if (req.param('id') != undefined){
         Sound.findOne(req.param('id'), function foundSound(err, sound) {
            if(process.env.PATH_SOUND_FILE === undefined){
                console.error('Error: You must define a valid .env file');
                process.exit(1);
            }
            var fullFilePath = process.env.PATH_SOUND_FILE+req.param('id');
            if (! fs.existsSync(fullFilePath)) {
                res.json({succes:false, message:'file not exist'});
            }
            var stat = fs.statSync(fullFilePath);
         
            res.writeHead(200, {
                'Content-Type': 'audio/mp3',
                'Content-Length': stat.size,
                'Accept-Ranges': 'bytes'
            });
            var readStream = fs.createReadStream(fullFilePath);
            readStream.pipe(res);
        });
    }
    else {
        res.json({succes:false, message: 'param is empty'});
    }
      },
};

