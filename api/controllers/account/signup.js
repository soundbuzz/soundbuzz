/**
 * TestController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

 let dotenv = require('dotenv');
 dotenv.config();

 if(process.env.URL === undefined){
   console.error('Error: You must define a valid .env file');
   process.exit(1);
 }

module.exports = {
    friendlyName: 'Signup',


    description: 'Signup user.',


    inputs: {
      email: {
        required: false,
        type: 'string',
        isEmail: true,
      },

      password: {
        required: false,
        type: 'string',
        maxLength: 200,
      },

      name:  {
        required: false,
        type: 'string',
      },

      surname:  {
        required: false,
        type: 'string',
      },

      login:  {
        required: false,
        type: 'string',
      },

      birthDate:  {
        required: false,
        type: 'string',
      },

      role:  {
        required: false,
        type: 'string',
      },

    },


    exits: {
      invalid: {
        responseType: 'badRequest',
        description: 'The provided fullName, password and/or email address are invalid.',
        extendedDescription: 'If this request was sent from a graphical user interface, the request '+
        'parameters should have been validated/coerced _before_ they were sent.'
      },

      emailAlreadyInUse: {
        statusCode: 409,
        description: 'The provided email address is already in use.',
      },

    },


    fn: async function (inputs, exits) {
      var newEmailAddress = inputs.email;
      var newUserRecord = await User.create(Object.assign({
        email: inputs.email,
        password: await sails.helpers.passwords.hashPassword(inputs.password),
        name: inputs.name,
        surname: inputs.surname,
        login : inputs.login,
        birthDate : inputs.birthDate,
        role: inputs.role,
        emailProofToken: await sails.helpers.strings.random('url-friendly'),
        emailProofTokenExpiresAt: Date.now() + sails.config.custom.emailProofTokenTTL,
        emailStatus: 'unconfirmed'
          }, ))
      .intercept('E_UNIQUE', 'emailAlreadyInUse')
      .fetch();

      //expfunvoie du mail de confirmation
       Mailer.sendWelcomeMail(newUserRecord, process.env.URL);

      return exits.success();

    }


};
