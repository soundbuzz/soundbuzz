module.exports = {


  friendlyName: 'Update password',


  description: 'Update the password for the logged-in user.',


  inputs: {

    email: {
      description: 'The email address of the alleged user who wants to recover their password.',
      example: 'rydahl@example.com',
      type: 'string',
      required: true
    }

  },


  fn: async function (inputs, exits) {
    let randomPassword = sails.helpers.strings.random();
    // Hash the new password.

    let hashed = await sails.helpers.passwords.hashPassword(randomPassword);

    // Update the record for the logged-in user.
    await User.update({ email: inputs.email })
    .set({
      password: hashed
    });

    var userRecord = await User.findOne({
      email: inputs.email,
    });

    Mailer.sendpasswordMail(userRecord, randomPassword);

    return exits.success();

  }


};
