/**
 * RecentsoundController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  listResent: async function (req, res) {
    if (req.param('nbr_page') != undefined ){
      let page = req.param('nbr_page')*10;
      var sounds = await Sound.find({actives: true})
    .sort([
      { createdAt: 'DESC' },]
    )
    .limit(page)
    .skip(page-10)
    ;
    return res.json(sounds);
  }
  else {
    res.json({succes:false, message: 'param is empty'});
  }
    }

};
