let dotenv = require('dotenv');
module.exports = {
    friendlyName: 'Sound',


    description: 'Sound',
  
  
    inputs: {

      title: {
        required: false,
        type: 'string',
        isEmail: true,
      },
  
      date: {
        required: false,
        type: 'string',
        maxLength: 200,
      },
  
      description:  {
        required: false,
        type: 'string',
      },
  
      photo:  {
        required: false,
        type: 'string',
      },
  
      artist:  {
        required: false,
        type: 'string',
      },

      song_writer:  {
        required: false,
        type: 'string',
      },

      explicit_content:  {
        required: false,
        type: 'string',
      },

      authorization_download:  {
        required: false,
        type: 'string',
      },

      transfer_date:  {
        required: false,
        type: 'string',
      },

      create_date:  {
        required: false,
        type: 'string',
      },

      duration_sound:  {
        required: false,
        type: 'string',
      },

      actives:  {
        required: false,
        type: 'boolean',
      },

      user_id:  {
        required: false,
        type: 'number',
      },
      
      genre_id:  {
        required: false,
        type: 'number',
      },
      
    },
  
  
    exits: {
      invalid: {
        responseType: 'badRequest',
        description: 'The provided fullName, password and/or email address are invalid.',
        extendedDescription: 'If this request was sent from a graphical user interface, the request '+
        'parameters should have been validated/coerced _before_ they were sent.'
      },
  
      emailAlreadyInUse: {
        statusCode: 409,
        description: 'The provided email address is already in use.',
      },
  
    },
  
  
    fn: async function (inputs, exits) {
        var newUserRecord = await User.create(Object.assign({
          email: inputs.email,
          password: await sails.helpers.passwords.hashPassword(inputs.password),
          name: inputs.name,
          surname: inputs.surname,
          login : inputs.login,
          birthDate : inputs.birthDate,
          rule: inputs.rule,
        }, ))
        .intercept('E_UNIQUE', 'emailAlreadyInUse')
        .fetch();
        return exits.success();
  
    }
  

};

