module.exports = {
    friendlyName: 'Sound',


    description: 'Sound',
  
  
    inputs: {
        id_sound: {
            required: true,
            type: 'number',
          },
      
    },
  
  
    exits: {
  
    },
  
  
    fn: async function (inputs, exits) {
        let newCommentList = await Comment.find({
            sound_id : inputs.id_sound
        });
        return exits.success(newCommentList);
    }
  

};

