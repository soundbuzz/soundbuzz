module.exports = {
  attributes: {
      title: {
          type: 'string',
          required: true,
          maxLength: 45,
        },
        description: {
          type: 'string',
          required: false,
          maxLength: 250,
        },
        photo: {
          type: 'string',
          columnType: 'text',
          required: false,
        },
        artist: {
          type: 'string',
          required: false,
          maxLength: 45,
        },
        song_writer: {
          type: 'string',
          required: false,
          maxLength: 45,
        },
        explicit_content: {
          type: 'boolean',
          required: false,
        },
        authorization_download: {
          type: 'string',
          required: false,
          maxLength: 45,
        },
        create_date: {
          type: 'string',
          columnType: 'datetime',
          required: false,
          maxLength: 45,
        },
        duration_sound: {
          type: 'string',
          required: false,
          maxLength: 45,
        },
        actives: {
          type: 'boolean',
          defaultsTo: false
        },
        nbr_view: {
          type: 'number',
          defaultsTo: 0
        },
        user_id: {
          required: true,
          model: 'user'
        },
        genre_id: {
          required: true,
          model: 'genre'
        },
        favorie: {
          collection: 'favorie',
          via: 'sound_id'
        },
        comment: {
          collection: 'comment',
          via: 'sound_id'
        },
        like: {
          collection: 'like',
          via: 'sound_id'
        },
        playlistsound: {
          collection: 'playlistsound',
          via: 'sound_id'
        },

  },
};
