module.exports = {
    attributes: {
        name: {
            type: 'string',
            required: true,
          },
          sound: {
            collection: 'sound',
            via: 'genre_id'
          }
    },
};