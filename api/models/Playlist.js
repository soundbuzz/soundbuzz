module.exports = {
    attributes: {
        name: {
            type: 'string',
            required: true,
            maxLength: 120,
          },
          isradio: {
            type: 'boolean',
            defaultsTo: false,
          },
          date: {
            model: 'user',
            required: true,
          },
          user_id: {
            model: 'user',
            required: true,
          },
          playlistsound: {
            collection: 'playlistsound',
            via: 'playlist_id'
          }
    },
};
