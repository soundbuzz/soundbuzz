module.exports = {
    attributes: {
          user_id: {
            model: 'user',
            required: true,
          },
          sound_id: {
            model: 'sound',
            required: true,
          }
    },
};