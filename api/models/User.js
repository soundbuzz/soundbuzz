module.exports = {
  attributes: {
    email: {
      type: 'string',
      required: true,
      unique: true,
      isEmail: true,
      maxLength: 200,
      example: 'carol.reyna@microsoft.com',
      columnType: 'varchar(100) CHARACTER SET utf8mb4'
    },
    name: {
      type: 'string',
      required: false,
      description: 'Full representation of the user\'s name',
      maxLength: 120,
      example: 'Lisa Microwave van der Jenny'
    },
    surname: {
      type: 'string',
      required: false,
      description: 'Full representation of the user\'s name',
      maxLength: 120,
      example: 'Lisa Microwave van der Jenny'
    },
    login: {
      type: 'string',
      required: true,
      description: 'Full representation of the user\'s name',
      unique: true,
      columnType: 'varchar(185)',
      example: 'Lisa Microwave van der Jenny'
    },
    password: {
      type: 'string',
      required: true,
      description: 'Securely hashed representation of the user\'s login password.',
      protect: true,
      example: '2$28a8eabna301089103-13948134nad'
    },
    authToken: {
      type: 'string',
      required: false,
      description: 'Secure token for auth',
    },
    authTokenExpiresAt: {
      type: 'number',
      description: 'A JS timestamp',
      example: 1502844074211
    },
    birthDate: {
      type: 'string',
      required: false,
    },
    emailProofToken: {
      type: 'string',
      description: 'A pseudorandom, probabilistically-unique token for use in our account verification emails.'
    },

    emailProofTokenExpiresAt: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the moment when this user\'s `emailProofToken` will expire (or 0 if the user currently has no such token).',
      example: 1502844074211
    },
    emailStatus: {
      type: 'string',
      isIn: ['unconfirmed', 'changeRequested', 'confirmed'],
      defaultsTo: 'confirmed',
      description: 'The confirmation status of the user\'s email address.',
      extendedDescription:
        `Users might be created as "unconfirmed" (e.g. normal signup) or as "confirmed" (e.g. hard-coded
      admin users).  When the email verification feature is enabled, new users created via the
      signup form have \`emailStatus: 'unconfirmed'\` until they click the link in the confirmation email.
      Similarly, when an existing user changes their email address, they switch to the "changeRequested"
      email status until they click the link in the confirmation email.`
    },

    emailChangeCandidate: {
      type: 'string',
      description: 'The (still-unconfirmed) email address that this user wants to change to.'
    },
    passwordResetToken: {
      type: 'string',
      description: 'A unique token used to verify the user\'s identity when recovering a password.  Expires after 1 use, or after a set amount of time has elapsed.'
    },

    passwordResetTokenExpiresAt: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the moment when this user\'s `passwordResetToken` will expire (or 0 if the user currently has no such token).',
      example: 1502844074211
    },
    lastSeenAt: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the moment at which this user most recently interacted with the backend while logged in (or 0 if they have not interacted with the backend at all yet).',
      example: 1502844074211
    },
    role: {
      type: 'number',
      defaultsTo: 0,
      required: false,
    },
    sound: {
      collection: 'sound',
      via: 'user_id'
    },
    favorie: {
      collection: 'favorie',
      via: 'user_id'
    },
    comment: {
      collection: 'comment',
      via: 'user_id'
    },
    like: {
      collection: 'like',
      via: 'user_id'
    },
    playlist: {
      collection: 'playlist',
      via: 'user_id'
    }
  },
};
