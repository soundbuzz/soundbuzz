module.exports = {
    attributes: {
          sound_id: {
            model: 'sound',
            required: true,
          },
          playlist_id: {
            model: 'playlist',
            required: true,
          },
    },
};