module.exports.sendWelcomeMail = function(obj, url) {
    sails.hooks.email.send(
        "welcomeEmail",
        {
            Name: obj.login,
            Url: url+'/api/v1/active/email?token='+obj.emailProofToken,
        },
        {
            to: obj.email,
            subject: 'Welcome to SoundBuzz'
        },
        function(err) {
            if(err) {
                console.log(err);
            }
            else {
                console.log("Ited!");
            }
        }
    )
};

module.exports.sendpasswordMail = function(obj, password) {
    sails.hooks.email.send(
        "PasswordEmail",
        {
            Name: obj.login,
            Password: password,
        },
        {
            to: obj.email,
            subject: 'Your new password SoundBuzz'
        },
        function(err) {
            if(err) {
                console.log(err);
            }
            else {
                console.log("Ited!");
            }
        }
    )
};
