# soundbuzz

### Start dev environment
- Clone project `git clone https://gitlab.com/soundbuzz/soundbuzz.git`
- Install dependencies `npm i`
- Start mysql server
- Create database for the project
- Create `.env` file by copying `.env.default`
- Run dev environment `npm start`
