const passport = require('passport'),
      LocalStrategy = require('passport-local').Strategy,
      bcrypt = require('bcrypt-nodejs');
passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});
passport.deserializeUser(function(id, cb){
  User.findOne({id}, function(err, user) {
    cb(err, users);
  });
});
passport.use(new LocalStrategy({
  loginField: 'login',
  passportField: 'password'
}, function(login, password, cb){
User.findOne({login: login}, function(err, user){
    if(err) return cb(err);
    if(!user) return cb(null, false, {message: 'Login not found'});
bcrypt.compare(password, user.password, function(err, res){
      if(!res) return cb(null, false, { message: 'Invalid Password' });
let userDetails = {
        email: user.email,
        login: user.login,
        id: user.id
      };
return cb(null, userDetails, { message: 'Login Succesful'});
    });
  });
}));