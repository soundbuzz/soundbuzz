//config/email.js
let dotenv = require('dotenv');
dotenv.config();

if(process.env.USER_MAIL === undefined || process.env.PASSWORD_MAIL === undefined){
  console.error('Error: You must define a valid .env file');
  process.exit(1);
}
module.exports.email = {
  host: 'smtp.gmail.com',
  port: 465,
    auth: {
        user: process.env.USER_MAIL,
        pass: process.env.PASSWORD_MAIL,
    },
    templateDir: "api/emailTemplates",
    testMode: false
};
