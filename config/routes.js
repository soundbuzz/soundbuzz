/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  // - APIs routes
  // -- Auth
  'POST /api/v1/sound/list-recent': 'Recentsound.listResent',
  'POST /api/v1/auth/login': { action: 'account/login' },
  'GET /api/v1/sound/download': 'Sounddownload.download',
  'POST /api/v1/sound/upload': 'Soundupload.upload',
  'POST /api/v1/sound/sound-detail': { action: 'sound/upload' },
  'POST /api/v1/sound/active': 'Activesound.active',
  'POST /api/v1/upload': 'SoundController.upload',
  '/logout': 'AuthController.logout',
  'GET /api/v1/active/email': { action: 'entrance/confirm-email' },
  'POST /api/v1/rescu/password': { action: 'account/rescu-password' },
  'DELETE /api/v1/sound/delete': { action: 'sound/delete' },
  'POST /api/v1/account/changepassword': 'Changepassword.changepassword',

  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
  // Note that, in this app, these API endpoints may be accessed using the `Cloud.*()` methods
  // from the CloudSDK library.
  'POST /api/v1/account/signup':                      { action: 'account/signup' } ,
  'POST /api/v1/content/sound':                      { action: 'content/sound' } ,
  'POST /api/v1/content/comment':                      { action: 'content/comment' } ,

  'GET /email/confirmed':    { view:   'email/confirmed' },

  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗  ╦═╗╔═╗╔╦╗╦╦═╗╔═╗╔═╗╔╦╗╔═╗
  //  ║║║║╚═╗║    ╠╦╝║╣  ║║║╠╦╝║╣ ║   ║ ╚═╗
  //  ╩ ╩╩╚═╝╚═╝  ╩╚═╚═╝═╩╝╩╩╚═╚═╝╚═╝ ╩ ╚═╝

};
